import { Component, OnInit } from '@angular/core';
import { TypeProcess } from '../models/typeProcess';
import { Campos } from '../models/campos';
import { EtapasProcesales } from '../models/etapasProcesales';
import { ListaChequeo } from '../models/listachequeo';
import { ChequeoTiposProcesos } from '../models/chequeoTiposProcesos';
import { EstadosPorTpoProceso } from '../models/estadosPorTipoProceso';
import { TypeProcessService } from '../services/typeProcess.service';
import { CamposService } from '../services/campos.service';
import { AuditoriaService } from '../services/auditoria.service';

import { EtapasProcesalesService } from '../services/etapasProcesales.service';
import { DatosServidor } from '../models/DatosServidor';

import { ListachequeoService } from '../services/listachequeo.services';

declare var swal: any;

@Component({
    selector: 'TypeProcess',
    templateUrl: '../views/typeProcess.component.html',
    styleUrls: ['../../assets/css/typeProcess-component.css'],
    providers: [TypeProcessService, CamposService, EtapasProcesalesService, ListachequeoService, AuditoriaService]


})

export class TypeProcessComponent implements OnInit {

    constructor(private _TypeProcessService: TypeProcessService, private _CamposService: CamposService, private _EtapasProcesalesService: EtapasProcesalesService, private _ListachequeoService: ListachequeoService, private _AuditoriaService: AuditoriaService) { }

    TypeProcessObject = new TypeProcess('');
    camposObject = new Campos([], 0);
    DatosServidorObject = new DatosServidor();
    DataTypeProcess = '';
    DataCampos = [];
    DataEstados = [];
    EtapaProcesalObject = new EtapasProcesales('', '');
    AgregarProceso = false;
    tipoProceso = '';
    BotonActualizar = false;
    cargarEstados = '';
    DataChequeo = [];
    ListachequeoObject = new ListaChequeo('');
    ChequeoTiposProcesosObject = new ChequeoTiposProcesos([], 0);
    EstadosTipoObject = new EstadosPorTpoProceso([], 0);
    DataEstadosTipo = '';
    dataJson = {};
    dataJsonCampos = {};
    dataJsonChequeo = {};
    cargando = false;
    recibodocumentos = false;
    dataUsuario = '';

    IndicesEtapasCheckeadas = {};


    ngOnInit() {

        this.SearchEtapasProceso();
        this.SearchTipo();
        this.SearchCampos();
        this.SearchChequeo();
        this.BuscarUsuario();
    }




    SaveTypeProcess() {

        try {

            if (this.TypeProcessObject.Nombre == '') {
                swal('Error', 'Ingrese el nombre!', 'error');
            }



            else {

                // console.log(this.TypeProcessObject.Nombre);

                this._TypeProcessService.SaveTypeProcess(this.DatosServidorObject.Url, this.TypeProcessObject).subscribe(
                    data => {
                        this._AuditoriaService.GuardarAuditoria(this.DatosServidorObject.Url, this.dataUsuario, "Guardar", "Insertar Tipo de proceso").subscribe(
                            data => {
                                swal('Correcto', 'Registro satisfactorio!', 'success');
                                this.SearchTipo();

                            },
                            error => {
                                alert('ERROR: -----------> ' + error);
                            },
                            () => {

                            }
                        );

                    },
                    error => {
                        alert('ERROR: -----------> ' + error);
                    },
                    () => {

                    }
                );

            }

        } catch (error) {

        }

    }


    SearchTipo() {

        try {

            this._TypeProcessService.SearchTypeProcess(this.DatosServidorObject.Url).subscribe(
                (data) => {

                    this.DataTypeProcess = data.value;
                    // console.log(JSON.stringify(data));
                },
                error => alert(error),
            );

        } catch (error) {

        }


    }



    SearchEtapasProceso() {

        try {

            this._EtapasProcesalesService.SearchEtapasProceso(this.DatosServidorObject.Url).subscribe(
                (data) => {

                    /*this.DataEstados = data.value;*/
                    this.ConstruirJsonEstados(data.value);

                    this.ConstruirOpcionesEtapProc();
                    // console.log(JSON.stringify(data));
                },
                error => alert(error),
            );

        } catch (error) {

        }



    }


    SaveEtapasProcesales() {

        try {

            if (this.EtapaProcesalObject.Nombre == '') {
                swal('Error', 'Ingrese la etapa!', 'error');
            }
            else if (/\D/.test(this.EtapaProcesalObject.Terminos)) {
                swal('Error', 'Los terminos deben ser numericos!', 'error');
            }



            else {


                this._EtapasProcesalesService.SaveEtapasProcesales(this.DatosServidorObject.Url, this.EtapaProcesalObject).subscribe(
                    data => {
                        this._AuditoriaService.GuardarAuditoria(this.DatosServidorObject.Url, this.dataUsuario, "Guardar", "Insertar Etapa procesal").subscribe(
                            data => {
                                swal('Correcto', 'Registro satisfactorio!', 'success');
                                this.SearchEtapasProceso();
                                this.EtapaProcesalObject = new EtapasProcesales('', '');

                            },
                            error => {
                                alert('ERROR: -----------> ' + error);
                            },
                            () => {

                            }
                        );

                    },
                    error => {
                        alert('ERROR: -----------> ' + error);
                    },
                    () => {

                    }
                );

            }

        } catch (error) {

        }

    }


    UpdateEtapas() {

        try {

            if (this.EtapaProcesalObject.Nombre == '') {
                swal('Error', 'Ingrese el nombre!', 'error');
            }

            else {


                this._EtapasProcesalesService.UpadateEtapasProcesales(this.DatosServidorObject.Url, this.EtapaProcesalObject).subscribe(
                    data => {
                        this._AuditoriaService.GuardarAuditoria(this.DatosServidorObject.Url, this.dataUsuario, "Modficiar", "Actualizar Etapa procesal").subscribe(
                            data => {
                                swal('Correcto', 'Modificación satisfactoria!', 'success');
                                this.SearchEtapasProceso();
                                this.BotonActualizar = false;

                            },
                            error => {
                                alert('ERROR: -----------> ' + error);
                            },
                            () => {

                            }
                        );

                    },
                    error => {
                        alert('ERROR: -----------> ' + error);
                    },
                    () => {

                    }
                );

            }

        } catch (error) {

        }

    }

    SaveCampos() {




        try {

            if (this.camposObject.IdCampos.length == 0) {
                swal('Error', 'Ingrese el campo!', 'error');
            }
            else if (this.camposObject.IdTiposProcesos == 0) {
                swal('Error', 'Seleccione el tipo de proceso!', 'error');
            }


            else {

                this.cargando = true;

                var contadoritems = 0;
                //consultamos las opciones que ya tenga asociados
                this._CamposService.buscarCamposTipos(this.DatosServidorObject.Url, this.tipoProceso).subscribe(

                    (data) => {

                        var info = data.value;

                        if (info.length != 0) {
                            var contadorDelete = 0;

                            for (let item of info) {

                                this._CamposService.BorrarCamposTipo(this.DatosServidorObject.Url, item.IdCamposTipos).subscribe(

                                    data => {
                                        contadorDelete = contadorDelete + 1;
                                        if (contadorDelete == info.length) {
                                            var contadoritems = 0;
                                            // alert(this.camposObject.IdCampos);

                                            for (let item of this.camposObject.IdCampos) {

                                                this._CamposService.SaveCampos(this.DatosServidorObject.Url, this.camposObject.IdTiposProcesos, item).subscribe(
                                                    data => {



                                                        contadoritems = contadoritems + 1;
                                                        if (contadoritems == this.camposObject.IdCampos.length) {


                                                            this._AuditoriaService.GuardarAuditoria(this.DatosServidorObject.Url, this.dataUsuario, "Modificar", "Actualizar Campos").subscribe(
                                                                data => {

                                                                    swal('Correcto', 'Registro satisfactorio!', 'success');
                                                                    this.cargando = false;
                                                                },
                                                                error => {
                                                                    alert('ERROR: -----------> ' + error);
                                                                },
                                                                () => {

                                                                }
                                                            );


                                                        }
                                                    },
                                                    error => {
                                                        alert('ERROR: -----------> ' + error);
                                                    },
                                                    () => {

                                                    }
                                                );
                                            }


                                        }
                                    }
                                );
                            }
                        }

                        else {

                            // alert(this.camposObject.IdCampos);
                            var contadoritems = 0;


                            for (let item of this.camposObject.IdCampos) {

                                this._CamposService.SaveCampos(this.DatosServidorObject.Url, this.camposObject.IdTiposProcesos, item).subscribe(
                                    data => {
                                        contadoritems = contadoritems + 1;
                                        ;
                                        if (contadoritems == this.camposObject.IdCampos.length) {
                                            swal('Correcto', 'Registro satisfactorio!', 'success');
                                            this.BuscarCamposTipo();
                                            this.cargando = false;

                                        }
                                    },
                                    error => {
                                        alert('ERROR: -----------> ' + error);
                                    },
                                    () => {

                                    }
                                );

                            }

                        }

                        error => {

                        }
                    }
                );
            }

        } catch (error) {

        }
    }


    // buscar campos

    SearchCampos() {

        try {

            this._CamposService.SearchCampos(this.DatosServidorObject.Url).subscribe(
                (data) => {

                    // this.DataCampos = data.value;
                    this.ConstruirJsonCampos(data.value);
                    this.ConstruirOpcionesCampos();

                },
                error => alert(error),
            );

        } catch (error) {

        }


    }

    ChangeCampo(IdCampo) {



        if (IdCampo.target.checked) {
            this.camposObject.IdCampos.push(IdCampo.target.value);
        }
        else {
            this.camposObject.IdCampos.splice(IdCampo, 1);
        }
    }



    CargarEstados(Nombre, Terminos, IdEstados) {

        this.EtapaProcesalObject = new EtapasProcesales(Nombre, Terminos, IdEstados);

        this.BotonActualizar = true;
    }



    // consultar lista chequeo

    SearchChequeo() {

        try {

            this._ListachequeoService.SearchChequeo(this.DatosServidorObject.Url).subscribe(
                (data) => {

                    this.DataChequeo = data.value;
                    // console.log(JSON.stringify(data));
                    this.ConstruirJsonChequeo(data.value);

                },
                error => alert(error),
            );

        } catch (error) {

        }


    }



    SaveListaChequeo() {

        try {

            if (this.ListachequeoObject.Nombre == '') {
                swal('Error', 'Ingrese el nombre!', 'error');
            }



            else {

                //  console.log(this.TypeProcessObject.Nombre);

                this._ListachequeoService.SaveListaChequeo(this.DatosServidorObject.Url, this.ListachequeoObject).subscribe(
                    data => {
                        this._AuditoriaService.GuardarAuditoria(this.DatosServidorObject.Url, this.dataUsuario, "Guardar", "Insertar Chequeo").subscribe(
                            data => {
                                swal('Correcto', 'Registro satisfactorio!', 'success');
                                this.SearchChequeo();

                            },
                            error => {
                                alert('ERROR: -----------> ' + error);
                            },
                            () => {

                            }
                        );

                    },
                    error => {
                        alert('ERROR: -----------> ' + error);
                    },
                    () => {

                    }
                );

            }

        } catch (error) {

        }

    }

    UpdateListasChequeo() {

        try {

            if (this.ListachequeoObject.Nombre == '') {
                swal('Error', 'Ingrese el Campo que desea agregar!', 'error');
            }

            else {
                console.log(this.TypeProcessObject.Nombre);



                this._ListachequeoService.UpadateListaChequeo(this.DatosServidorObject.Url, this.ListachequeoObject).subscribe(
                    data => {
                        this._AuditoriaService.GuardarAuditoria(this.DatosServidorObject.Url, this.dataUsuario, "Modificar", "Actualizar Chequeo").subscribe(
                            data => {
                                swal('Correcto', 'Modificación satisfactoria!', 'success');
                                this.SearchChequeo();
                                this.BotonActualizar = false;

                            },
                            error => {
                                alert('ERROR: -----------> ' + error);
                            },
                            () => {

                            }
                        );

                    },
                    error => {
                        alert('ERROR: -----------> ' + error);
                    },
                    () => {

                    }
                );

            }

        } catch (error) {

        }

    }

    CargarChequeo(Nombre, IdChequeo) {


        this.ListachequeoObject = new ListaChequeo(Nombre, IdChequeo);

        this.BotonActualizar = true;
    }


    ChangeChequeo(IdChequeo) {



        if (IdChequeo.target.checked) {
            this.ChequeoTiposProcesosObject.IdChequeo.push(IdChequeo.target.value);
        }
        else {
            this.ChequeoTiposProcesosObject.IdChequeo.splice(IdChequeo, 1);
        }
    }



    ChequeoTiposProcesos() {


        try {

            if (this.ChequeoTiposProcesosObject.IdChequeo == 0) {
                swal('Error', 'Ingrese el campo de chequeo!', 'error');
            }

            else if (this.ChequeoTiposProcesosObject.IdTiposProcesos == 0) {
                swal('Error', 'Seleccione el tipo de proceso!', 'error');
            }



            else {

                // alert(this.ChequeoTiposProcesosObject.IdChequeo);
                this.cargando = true;
                this._ListachequeoService.buscarChequeoTipos(this.DatosServidorObject.Url, this.tipoProceso).subscribe(

                    (data) => {

                        // console.log(JSON.stringify(data.value));

                        var contadoritems = 0;

                        var info = data.value;

                        if (info.length != 0) {

                            var contadorDelete = 0;


                            for (let item of info) {
                                this._ListachequeoService.BorrarChequeosTipo(this.DatosServidorObject.Url, item.IdChequeoTipo).subscribe(
                                    data => {
                                        contadorDelete = contadorDelete + 1;

                                        if (contadorDelete == info.length) {


                                            var contadoritems = 0;


                                            for (let item of this.ChequeoTiposProcesosObject.IdChequeo) {

                                                this._ListachequeoService.ChequeoTiposProcesos(this.DatosServidorObject.Url, this.ChequeoTiposProcesosObject.IdTiposProcesos, item).subscribe(
                                                    data => {
                                                        contadoritems = contadoritems + 1;
                                                        ;
                                                        if (contadoritems == this.ChequeoTiposProcesosObject.IdChequeo.length) {

                                                            this._AuditoriaService.GuardarAuditoria(this.DatosServidorObject.Url, this.dataUsuario,"Modificar","Actualizar Lista de chequeo por tipo de proceso").subscribe(
                                                                data => {

                                                                    swal('Correcto', 'Registro satisfactorio!', 'success');
                                                                    this.BuscarChequeo(this.tipoProceso);
                                                                    this.cargando = false;
                                                                },
                                                                error => {
                                                                    alert('ERROR: -----------> ' + error);
                                                                },
                                                                () => {

                                                                }
                                                            );

                                                        }
                                                    },
                                                    error => {
                                                        alert('ERROR: -----------> ' + error);
                                                    },
                                                    () => {

                                                    }
                                                );

                                            }

                                        }
                                    },
                                    error => {
                                        console.log('Error al borrar');
                                    }
                                );
                            }

                        }

                        else {

                            var contadoritems = 0;


                            for (let item of this.ChequeoTiposProcesosObject.IdChequeo) {

                                this._ListachequeoService.ChequeoTiposProcesos(this.DatosServidorObject.Url, this.ChequeoTiposProcesosObject.IdTiposProcesos, item).subscribe(
                                    data => {
                                        contadoritems = contadoritems + 1;
                                        ;
                                        if (contadoritems == this.ChequeoTiposProcesosObject.IdChequeo.length) {
                                            swal('Correcto', 'Registro satisfactorio!', 'success');
                                            this.BuscarChequeo(this.tipoProceso);
                                            this.cargando = false;
                                        }
                                    },
                                    error => {
                                        alert('ERROR: -----------> ' + error);
                                    },
                                    () => {

                                    }
                                );

                            }

                        }

                    }
                );
            }

        } catch (error) {

        }
    }

    CancelarChequeo() {

        this.BotonActualizar = false;
        this.ListachequeoObject = new ListaChequeo('');

    }

    CancelarEstado() {
        this.BotonActualizar = false;
        this.EtapaProcesalObject = new EtapasProcesales('', '');
    }

    CancelarTipo() {

        this.AgregarProceso = false;
    }





    // tipo proceso por estados procesales //////////////////////////////////////////////////


    ChangeEstadosTipo(IdEstados, Indice) {

        if (IdEstados.target.checked) {

            // this.EstadosTipoObject.IdEstados.push(IdEstados.target.value);
            
            this.EstadosTipoObject.IdEstados[IdEstados.target.value] =  '';

            if (IdEstados.target.value == 1) {

                this.recibodocumentos = true;


            }

            var ContadorElementos = 0;

            for (let item of Object.keys(this.EstadosTipoObject.IdEstados)) {

                ContadorElementos++;

            }

            this.IndicesEtapasCheckeadas[Indice] = Indice;
            this.DataEstados[Indice].Prioridad = ContadorElementos;

            this.EstadosTipoObject.IdEstados[IdEstados.target.value] = ContadorElementos;
        }
        else {

            console.log(JSON.stringify(this.IndicesEtapasCheckeadas));
            //  this.EstadosTipoObject.IdEstados.splice(IdEstados, 1);
            delete this.EstadosTipoObject.IdEstados[IdEstados.target.value];

            delete this.IndicesEtapasCheckeadas[Indice];

            console.log(JSON.stringify(this.IndicesEtapasCheckeadas));
            if (IdEstados.target.value == 1) {

                this.recibodocumentos = false;
                this.EstadosTipoObject.IdEstados = {};

                // this.IndicesEtapasCheckeadas = [];

                //LIMPIAMOS TODAS LAS PRIORIDADES

                var ContadorEstados = 0;

                for (let item of this.DataEstados) {

                    this.DataEstados[ContadorEstados].Prioridad = '';
                    ContadorEstados++;
                }


            }

            else {
                
                this.DataEstados[Indice].Prioridad = '';

                var ContadorPrioridades = 0;

                for (let item of Object.keys(this.IndicesEtapasCheckeadas)) {

                    ContadorPrioridades++;

                    this.DataEstados[item].Prioridad = ContadorPrioridades;

                }

            }


        }
    }


    // Guardar estados x tipo//////////////////////////////////////////////

    GuardarEstadosTipo() {

        
        try {

            if (this.EstadosTipoObject.IdEstados == 0) {
                swal('Error', 'Ingrese el estado!', 'error');
            }

            else if (this.EstadosTipoObject.IdTiposProcesos == 0) {
                swal('Error', 'Seleccione el tipo de proceso!', 'error');
            }



            else {

                this.cargando = true;

                //////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //consultamos las opciones que ya tenga asociados
                this._TypeProcessService.buscarEstadosTipos(this.DatosServidorObject.Url, this.tipoProceso).subscribe(

                    (data) => {

                        var info = data.value;

                        if (info.length != 0) {

                            var contadorDelete = 0;

                            for (let item of info) {
                                this._TypeProcessService.BorrarEstadosTipo(this.DatosServidorObject.Url, item.IdEstadosTipos).subscribe(
                                    data => {
                                        contadorDelete = contadorDelete + 1;

                                        if (contadorDelete == info.length) {


                                            var contadoritems = 0;


                                            for (let item of Object.keys(this.EstadosTipoObject.IdEstados)) {

                                                this._TypeProcessService.EstadosTipo(this.DatosServidorObject.Url, this.EstadosTipoObject.IdTiposProcesos, item, this.EstadosTipoObject.IdEstados[item]).subscribe(
                                                    data => {
                                                        contadoritems = contadoritems + 1;
                                                        ;
                                                        if (contadoritems == Object.keys(this.EstadosTipoObject.IdEstados).length) {
                                                            swal('Correcto', 'Registro satisfactorio!', 'success');
                                                            this.buscarEstadosTipos();
                                                            this.cargando = false;
                                                        }
                                                    },
                                                    error => {
                                                        alert('ERROR: -----------> ' + error);
                                                    },
                                                    () => {

                                                    }
                                                );

                                            }

                                        }
                                    },
                                    error => {
                                        console.log('Error al borrar');
                                    }
                                );
                            }
                        }
                        else {

                            var contadoritems = 0;


                            for (let item of Object.keys(this.EstadosTipoObject.IdEstados)) {

                                this._TypeProcessService.EstadosTipo(this.DatosServidorObject.Url, this.EstadosTipoObject.IdTiposProcesos, item, this.EstadosTipoObject.IdEstados[item]).subscribe(
                                    data => {
                                        contadoritems = contadoritems + 1;
                                        ;
                                        if (contadoritems == Object.keys(this.EstadosTipoObject.IdEstados).length) {
                                            swal('Correcto', 'Registro satisfactorio!', 'success');
                                            this.buscarEstadosTipos();
                                            this.cargando = false;
                                        }
                                    },
                                    error => {
                                        alert('ERROR: -----------> ' + error);
                                    },
                                    () => {

                                    }
                                );

                            }

                        }


                    },
                    error => alert(error),
                );
                /////////////////////////////////////////////////////////////////////////////////////////////

                //Borramos todos los registros





            }

        } catch (error) {

        }
    }


    // buscar estados tipos proceso  ///////////////////////////////////
    //////////////////////////////////////////////////////////////

    buscarEstadosTipos() {

        try {

            // alert(this.tipoProceso);
            this._TypeProcessService.buscarEstadosTipos(this.DatosServidorObject.Url, this.tipoProceso).subscribe(
                (data) => {

                    console.log(JSON.stringify(data.value));
                    // this.DataEstadosTipo = data.value;
                    this.DataEstados = [];
                    this.ObtenerOpcionesChequedas(data.value);
                    this.ConstruirOpcionesEtapProc();
                },
                error => alert(error),
            );

        } catch (error) {

        }


    }

    ConstruirJsonEstados(foretapasproceso) {

        this.dataJson = {};

        for (let item of foretapasproceso) {

            this.dataJson[item.IdEstados] = { IdEstados: item.IdEstados, checked: false, termino: item.Terminos, nombre: item.Nombre, Prioridad: '' };

        }


    }

    ConstruirOpcionesEtapProc() {

        this.DataEstados = [];

        var ObjDatos = Object.keys(this.dataJson);

        for (let item of ObjDatos) {
            this.DataEstados.push(this.dataJson[item]);
        }
    }


    ObtenerOpcionesChequedas(Data) {

        //Recorremos todas las opciones y las cambiamos a false, para que no nos queden opciones checkeadas en principio
        var ObjDatos = Object.keys(this.dataJson);

        for (let item of ObjDatos) {
            this.dataJson[item].checked = false;
        }

        //Ahora si cambiamos a true las opciones relacionadas al idtipoproceso seleccionado
        this.EstadosTipoObject.IdEstados = {};

        this.IndicesEtapasCheckeadas = {};
        this.recibodocumentos = true;

        var i = 0;

        for (let item of Data) {


            this.dataJson[item.IdEstados].checked = true;
            this.dataJson[item.IdEstados].Prioridad = item.Posicion;

            this.EstadosTipoObject.IdEstados[item.IdEstados] = item.Posicion;

            this.IndicesEtapasCheckeadas[i] = i;

            i++;
        }
    }

    // fin editar estados por tipo proceso/////////////////////////////////////




    // INICIO EDITAR CAMPOS POR TIPO DE PROCESO//////////////////////////////

    BuscarCamposTipo() {

        try {

            this._CamposService.buscarCamposTipos(this.DatosServidorObject.Url, this.tipoProceso).subscribe(
                (data) => {

                    // this.DataEstadosTipo = data.value;
                    this.DataCampos = [];
                    this.ObtenerOpcionesChequedasCampos(data.value);
                    this.ConstruirOpcionesCampos();
                },
                error => alert(error),
            );

        } catch (error) {

        }


    }


    ConstruirJsonCampos(forcampos) {

        for (let item of forcampos) {

            this.dataJsonCampos[item.IdCampos] = { IdCampos: item.IdCampos, checked: false, Nombre: item.Nombre };

        }


    }

    ConstruirOpcionesCampos() {

        var ObjDatos = Object.keys(this.dataJsonCampos);

        for (let item of ObjDatos) {
            this.DataCampos.push(this.dataJsonCampos[item]);
        }
    }

    ObtenerOpcionesChequedasCampos(Data) {



        //Recorremos todas las opciones y las cambiamos a false, para que no nos queden opciones checkeadas en principio
        var ObjDatos = Object.keys(this.dataJsonCampos);

        for (let item of ObjDatos) {

            this.dataJsonCampos[item].checked = false;
        }

        this.camposObject.IdCampos = [];
        for (let item of Data) {
            this.dataJsonCampos[item.IdCampos].checked = true;

            this.camposObject.IdCampos.push(item.IdCampos);
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////


    BuscarChequeo(TipoProceso) {

        try {

            this._ListachequeoService.buscarChequeoTipos(this.DatosServidorObject.Url, TipoProceso).subscribe(
                (data) => {
                    // this.DataEstadosTipo = data.value;
                    //console.log(JSON.stringify(data.value));
                    this.DataChequeo = [];
                    this.ObtenerOpcionesChequedasLista(data.value);
                    this.ConstruirOpcionesChequeo();
                },
                error => alert(error),
            );

        } catch (error) {

        }


    }

    ConstruirJsonChequeo(forcampos) {

        for (let item of forcampos) {

            this.dataJsonChequeo[item.IdChequeo] = { IdChequeo: item.IdChequeo, checked: false, Nombre: item.Nombre };

        }


    }


    ConstruirOpcionesChequeo() {

        var ObjDatos = Object.keys(this.dataJsonChequeo);

        for (let item of ObjDatos) {
            /*this.ChequeoTiposProcesosObject.IdChequeo.push(this.dataJsonChequeo[item]);*/
            this.DataChequeo.push(this.dataJsonChequeo[item]);
        }
    }

    ObtenerOpcionesChequedasLista(Data) {



        //Recorremos todas las opciones y las cambiamos a false, para que no nos queden opciones checkeadas en principio
        var ObjDatos = Object.keys(this.dataJsonChequeo);

        for (let item of ObjDatos) {
            this.dataJsonChequeo[item].checked = false;
        }

        this.ChequeoTiposProcesosObject.IdChequeo = [];

        for (let item of Data) {
            this.dataJsonChequeo[item.IdChequeo].checked = true;

            this.ChequeoTiposProcesosObject.IdChequeo.push(item.IdChequeo);
        }
    }

    validarcampo(dato) {

        var salida;

        // switch(tipodecampo){
        //     case 'number':
        salida = dato.replace(/[^0-9\.]+/g, '');
        //     break;
        // }

        return salida;
    }


    BuscarUsuario() {

        try {

            this._AuditoriaService.BuscarUsuario(this.DatosServidorObject.Url, 1).subscribe(
                (data) => {

                    this.dataUsuario = data.value[0].Nombre;

                },
                error => alert(error),
            );

        } catch (error) {

        }


    }




}

