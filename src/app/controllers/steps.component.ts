import { Component, OnInit } from '@angular/core';
import { Contrato } from '../models/contrato';
import { Proyecto } from '../models/proyecto';
import { ContratoService } from '../services/contrato.service';
import { ProyectoService } from '../services/proyecto.service';
import { AuditoriaService } from '../services/auditoria.service';
import { DatosServidor } from '../models/DatosServidor';

declare var swal: any;

@Component({
    selector: 'Steps',
    templateUrl: '../views/steps.component.html',
    styleUrls: ['../../assets/css/steps-component.css'],
    providers: [ContratoService, ProyectoService, AuditoriaService]


})

export class StepsComponent implements OnInit {


    datacliente = '';
    datacontrato = [];

    DatosContratos;
    DataProyectos

    datacontratoproyecto = '';
    ContratoObject = new Contrato('', '', '');
    ProyectoObject = new Proyecto('', '', '', '');
    BotonActualizar = false;
    dataproyectos = [];
    dataUsuario = '';

    constructor(private _ContratoService: ContratoService, private _ProyectoService: ProyectoService, private _AuditoriaService: AuditoriaService) { }


    ngOnInit() {

        this.BuscarPersona();
        this.BuscarContrato();
        this.BuscarContratoXProyecto();
        this.BuscarProyectos();
        this.BuscarUsuario();

    }

    DatosServidorObject = new DatosServidor();


    BuscarPersona() {

        try {

            this._ContratoService.BuscarPersona(this.DatosServidorObject.Url).subscribe(
                (data) => {

                    this.datacliente = data.value;
                    // console.log(JSON.stringify(data));
                },
                error => alert(error),
            );

        } catch (error) {

        }

    }




    GuardarContrato() {

        try {

            if (this.ContratoObject.Contrato == '') {
                swal('Error', 'Ingrese el Contrato!', 'error');
            }


            else if (this.ContratoObject.Descripcion == '') {
                swal('Error', 'Ingrese la descripción!', 'error');
            }

            else if (!/^[a-zA-Z 1-9 . á é í ó ú]*$/g.test(this.ContratoObject.Descripcion)) {
                swal('Error', 'el campo descripción solo admite letras!', 'error');
            }

            else if (this.ContratoObject.IdPersona == '') {
                swal('Error', 'Ingrese la descripción!', 'error');
            }

            else {


                this._ContratoService.GuardarContrato(this.DatosServidorObject.Url, this.ContratoObject).subscribe(
                    data => {
                        this._AuditoriaService.GuardarAuditoria(this.DatosServidorObject.Url, this.dataUsuario, "Registrar", "Insertar Contrato").subscribe(
                            data => {
                                swal('Correcto', 'Registro satisfactorio!', 'success');
                                this.BuscarContrato();

                            },
                            error => {
                                alert('ERROR: -----------> ' + error);
                            },
                            () => {

                            }
                        );

                    },
                    error => {
                        alert('ERROR: -----------> ' + error);
                    },
                    () => {

                    }
                );

            }

        } catch (error) {

        }

    }

    CancelarContrato() {

        this.BotonActualizar == false;
        this.ContratoObject = new Contrato('', '', '');
    }

    BuscarContrato() {

        try {

            this.datacontrato = [];

            this._ContratoService.BuscarContrato(this.DatosServidorObject.Url).subscribe(
                (data) => {

                    
                    //  console.log(JSON.stringify(data));

                    // console.log(JSON.stringify(data.value));

                    var ContadorContratos = 0;
                    var NumContratos = data.value.length;


                    for (let itemContrato of data.value) {

                        

                        this._ContratoService.BuscarNumeroDePersona(this.DatosServidorObject.Url, itemContrato.IdPersona).subscribe(
                            (data) => {

                                ContadorContratos++;



                                //console.log(JSON.stringify(data.value));

                                var jsonEntrada = { Contrato: itemContrato.Contrato, Descripcion: itemContrato.Descripcion, NumeroIdentidad: data.value[0].NumeroIdentidad, IdContrato: itemContrato.IdContrato, IdPersona: itemContrato.IdPersona };

                                //console.log(JSON.stringify(jsonEntrada));

                                this.datacontrato.push(jsonEntrada);

                                // console.log(JSON.stringify(this.datacontrato));


                                if(ContadorContratos == NumContratos){
                                    this.DatosContratos = this.datacontrato;
                                }

                            }


                        )

                    }

                    

                },
                error => alert(error),
            );


        } catch (error) {

        }

    }

    CargarContrato(d) {

        // console.log(JSON.stringify(d));
        this.ContratoObject = new Contrato(d.IdPersona, d.Contrato, d.Descripcion, d.IdContrato);

        this.BotonActualizar = true;
    }




    ActualizarContrato() {

        try {

            if (this.ContratoObject.Contrato == '') {
                swal('Error', 'Ingrese el Contrato!', 'error');
            }

            else if (!/^[a-zA-Z 1-9 . á é í ó ú]*$/g.test(this.ContratoObject.Contrato)) {
                swal('Error', 'el campo contrato solo admite letras o números!', 'error');
            }


            else if (this.ContratoObject.Descripcion == '') {
                swal('Error', 'Ingrese la descripción!', 'error');
            }


            else if (!/^[a-zA-Z 1-9 . á é í ó ú]*$/g.test(this.ContratoObject.Descripcion)) {
                swal('Error', 'el campo descripción solo admite letras o números!', 'error');
            }

            else if (this.ContratoObject.IdPersona == '') {
                swal('Error', 'Ingrese la descripción!', 'error');
            }

            else {


                this._ContratoService.ActualizarContrato(this.DatosServidorObject.Url, this.ContratoObject).subscribe(
                    data => {
                        this._AuditoriaService.GuardarAuditoria(this.DatosServidorObject.Url, this.dataUsuario, "Modificar", "Actualizar Contrato").subscribe(
                            data => {
                                swal('Correcto', 'Modificación satisfactoria!', 'success');
                                this.BuscarContrato();
                                this.ContratoObject = new Contrato('', '', '');

                            },
                            error => {
                                alert('ERROR: -----------> ' + error);
                            },
                            () => {

                            }
                        );

                    },
                    error => {
                        alert('ERROR: -----------> ' + error);
                    },
                    () => {

                    }
                );

            }

        } catch (error) {

        }

    }


    CancelarContrato2() {

        this.BotonActualizar == false;
        this.ContratoObject = new Contrato('', '', '');
    }


    // PROYECTOOOOOOOOOOOO


    GuardarProyecto() {

        try {


            if (this.ProyectoObject.IdContratos == '') {
                swal('Error', 'Ingrese el Contrato!', 'error');
            }


            else if (this.ProyectoObject.NombreProyecto == '') {
                swal('Error', 'Ingrese el nombre!', 'error');
            }

            else if (!/^[a-zA-Z 1-9 . á é í ó ú]*$/g.test(this.ProyectoObject.NombreProyecto)) {
                swal('Error', 'el campo Nombre de proyecto solo admite letras o números!', 'error');
            }

            else if (this.ProyectoObject.Interventor == '') {
                swal('Error', 'Ingrese el interventor!', 'error');
            }


            else if (!/^[a-zA-Z . á é í ó ú]*$/g.test(this.ProyectoObject.Interventor)) {
                swal('Error', 'el campo Interventor solo admite letras!', 'error');
            }

            else if (this.ProyectoObject.Representador == '') {
                swal('Error', 'Ingrese el reprentador!', 'error');
            }

            else if (!/^[a-zA-Z . á é í ó ú]*$/g.test(this.ProyectoObject.Interventor)) {
                swal('Error', 'el campo Representador solo admite letras!', 'error');
            }

            else {


                this._ProyectoService.GuardarProyecto(this.DatosServidorObject.Url, this.ProyectoObject).subscribe(
                    (data) => {
                        this._AuditoriaService.GuardarAuditoria(this.DatosServidorObject.Url, this.dataUsuario, "Guardar", "Insertar Proyecto").subscribe(
                            (data) => {
                                swal('Correcto', 'Registro satisfactorio!', 'success');
                                this.BuscarProyectos();

                            },
                            (error) => {
                                alert('ERROR: -----------> ' + error);
                            },
                            () => {

                            }
                        );

                    },
                    (error) => {
                        alert('ERROR: -----------> ' + error);
                    },
                    () => {

                    }
                );

            }

        } catch (error) {

        }

    }



    BuscarContratoXProyecto() {

        try {

            this._ProyectoService.BuscarContrato(this.DatosServidorObject.Url).subscribe(
                (data) => {

                    this.datacontratoproyecto = data.value;
                    // console.log(JSON.stringify(data));
                },
                error => alert(error),
            );

        } catch (error) {

        }

    }




    ActualizarProyecto() {

        try {


            if (this.ProyectoObject.IdContratos == '') {
                swal('Error', 'Ingrese el Contrato!', 'error');
            }


            else if (this.ProyectoObject.NombreProyecto == '') {
                swal('Error', 'Ingrese el nombre!', 'error');
            }

            else if (!/^[a-zA-Z 1-9 . á é í ó ú]*$/g.test(this.ProyectoObject.NombreProyecto)) {
                swal('Error', 'el campo Nombre de proyecto solo admite letras o números!', 'error');
            }

            else if (this.ProyectoObject.Interventor == '') {
                swal('Error', 'Ingrese el interventor!', 'error');
            }


            else if (!/^[a-zA-Z . á é í ó ú]*$/g.test(this.ProyectoObject.Interventor)) {
                swal('Error', 'el campo Interventor solo admite letras!', 'error');
            }

            else if (this.ProyectoObject.Representador == '') {
                swal('Error', 'Ingrese el reprentador!', 'error');
            }

            else if (!/^[a-zA-Z . á é í ó ú]*$/g.test(this.ProyectoObject.Interventor)) {
                swal('Error', 'el campo Representador solo admite letras!', 'error');
            }

            else {


                this._ProyectoService.ActualizarProyecto(this.DatosServidorObject.Url, this.ProyectoObject).subscribe(
                    (data) => {
                        this._AuditoriaService.GuardarAuditoria(this.DatosServidorObject.Url, this.dataUsuario, "Modificar", "Actualizar Proyecto").subscribe(
                            (data) => {
                                swal('Correcto', 'Modificación satisfactoria!', 'success');
                                this.BuscarProyectos();
                                this.ProyectoObject = new Proyecto('', '', '', '');
                                this.BotonActualizar = false;


                            },
                            (error) => {
                                alert('ERROR: -----------> ' + error);
                            },
                            () => {

                            }
                        );


                    },
                    (error) => {
                        alert('ERROR: -----------> ' + error);
                    },
                    () => {

                    }
                );

            }

        } catch (error) {

        }

    }


    BuscarProyectos() {

        try {

            this._ProyectoService.BuscarProyecto(this.DatosServidorObject.Url).subscribe(
                (data) => {

                    this.dataproyectos = [];
                    //  console.log(JSON.stringify(data.value));


                    for (let itemProyecto of data.value) {

                        var ContadorProyectos=0;
                        var NumProyectos=data.value.length;


                        // console.log(itemProyecto.IdContratos);

                        this._ProyectoService.BuscarNombreContrato(this.DatosServidorObject.Url, itemProyecto.IdContratos).subscribe(
                            (data) => {

                                ContadorProyectos++;

                                var jsonEntrada = { Contrato: data.value[0].Contrato, NombreProyecto: itemProyecto.NombreProyecto, Interventor: itemProyecto.Interventor, Representador: itemProyecto.Representador, IdProyectos: itemProyecto.IdProyectos, IdContratos: itemProyecto.IdContratos };

                                this.dataproyectos.push(jsonEntrada);

                                //  console.log(JSON.stringify(this.datacontrato));

                                if(ContadorProyectos==NumProyectos){
                                    this.DataProyectos= this.dataproyectos;
                                }
                            }



                        )


                    }
                },
                error => alert(error),
            );

        } catch (error) {

        }

    }


    CargarProyecto(d) {

        console.log(JSON.stringify(d));

        this.ProyectoObject = new Proyecto(d.IdContratos, d.NombreProyecto, d.Interventor, d.Representador, d.IdProyectos);

        this.BotonActualizar = true;
    }

    cancelarproyecto() {

        this.ProyectoObject = new Proyecto('', '', '', '');
    }

    cancelarproyecto2() {

        this.BotonActualizar = false;
        this.ProyectoObject = new Proyecto('', '', '', '');
    }


    BuscarUsuario() {

        try {

            this._AuditoriaService.BuscarUsuario(this.DatosServidorObject.Url, 1).subscribe(
                (data) => {

                    this.dataUsuario = data.value[0].Nombre;

                },
                error => alert(error),
            );

        } catch (error) {

        }


    }


}