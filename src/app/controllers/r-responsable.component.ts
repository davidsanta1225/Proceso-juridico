import { Component, OnInit } from '@angular/core';
import { ResponsableService } from '../services/r-responsable.service';
import { DatosServidor } from '../models/DatosServidor';


@Component({
    selector: 'r-responsable',
    templateUrl: '../views/r-responsable.component.html',
    styleUrls: ['../../assets/css/r-responsable-component.css'],
    providers: [ResponsableService]

})

export class RresponsableComponent implements OnInit {

    constructor(private _ResponsableService: ResponsableService) { }


    dataResponsable = '';
    DatosServidorObject = new DatosServidor();


    ngOnInit() {

        //this.ReporteCliente();
    }
    
    // ReporteCliente() {

    //     try {

    //         this._ResponsableService.ReporteClienteXUsuario(this.DatosServidorObject.Url).subscribe(
    //             (data) => {

    //                 this.dataResponsable = data.value;
    //                 // console.log(JSON.stringify(data));
    //             },
    //             error => alert(error),
    //         );

    //     } catch (error) {

    //     }


    // }

}