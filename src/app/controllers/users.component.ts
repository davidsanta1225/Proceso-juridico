import { Component, OnInit } from '@angular/core';
import { Users } from '../models/users';
import { UsersService } from '../services/users.service';
import { DatosServidor } from '../models/DatosServidor';
import { AuditoriaService } from '../services/auditoria.service';
//import swal from 'sweetalert2';


declare var swal: any;


@Component({
    selector: 'Users',
    templateUrl: '../views/users.component.html',
    styleUrls: ['../../assets/css/users-component.css'],
    providers: [UsersService, AuditoriaService],
})

export class UsersComponent implements OnInit {

    constructor(private _UsersService: UsersService, private _AuditoriaService: AuditoriaService) { }

    UsersObject = new Users('', '', '', '', '', '', '', '');
    DatosServidorObject = new DatosServidor();
    cargarUsuarios = '';

    DataUsers = '';
    dataUsuario = '';

    BotonActualizar = false;

    ngOnInit() {

        this.SearchUsers();
        this.BuscarUsuario();

    }




    SaveUsers() {

        try {

            if (this.UsersObject.Nombre == '') {
                swal('Error', 'Ingrese el nombre!', 'error');
            }
            else if (!/^[a-zA-Z á é í ó ú Á É Í Ó Ú]+$/g.test(this.UsersObject.Nombre)) {
                swal('Error', 'El campo nombre solo admite letras!', 'error');
            }

            else if (this.UsersObject.Apellidos == '') {
                swal('Error', 'Ingrese los apellidos!', 'error');
            }

            else if (!/^[a-zA-Z á é í ó ú Á É Í Ó Ú]+$/g.test(this.UsersObject.Apellidos)) {
                swal('Error', 'El campo apellido solo admite letras!', 'error');
            }

            else if (this.UsersObject.Documentos == '') {
                swal('Error', 'Ingrese el documento!', 'error');
            }
            else if (this.UsersObject.Cargo == '') {
                swal('Error', 'Ingrese el cargo!', 'error');
            }
            else if (this.UsersObject.CorreoElectronico == '') {
                swal('Error', 'Ingrese el correo electronico!', 'error');
            }

            else if (!/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(this.UsersObject.CorreoElectronico)) {
                swal('Error', 'El correo ingresado no es valido!', 'error');

            }
            else if (this.UsersObject.Password == '') {
                swal('Error', 'Ingrese la contraseña!', 'error');

            }
            else if (this.UsersObject.ConfirmPassword != this.UsersObject.Password) {

                swal('Error', 'Las contraseñas no coinciden!', 'error');
            }


            else if (this.UsersObject.IdRol == '') {
                swal('Error', 'Ingrese los rol!', 'error');
            }


            else {


                this._UsersService.SaveUsers(this.DatosServidorObject.Url, this.UsersObject).subscribe(
                    data => {
                        this._AuditoriaService.GuardarAuditoria(this.DatosServidorObject.Url, this.dataUsuario, "Guardar", "Insertar Usuario").subscribe(
                            data => {
                                swal('Correcto', 'Registro satisfactorio!', 'success');
                                this.SearchUsers();
                            },
                            error => {
                                alert('ERROR: -----------> ' + error);
                            },
                            () => {

                            }
                        );
                    },
                    error => {
                        alert('ERROR: -----------> ' + error);
                    },
                    () => {

                    }
                );

            }

        } catch (error) {

        }

    }




    UpdateUsers() {

        try {

            if (this.UsersObject.Nombre == '') {
                swal('Error', 'Ingrese el nombre!', 'error');
            }

            else if (!/^[a-zA-Z á é í ó ú Á É Í Ó Ú]+$/g.test(this.UsersObject.Nombre)) {
                swal('Error', 'El campo nombre solo admite letras!', 'error');
            }

            else if (this.UsersObject.Apellidos == '') {
                swal('Error', 'Ingrese los apellidos!', 'error');
            }

            else if (!/^[a-zA-Z á é í ó ú Á É Í Ó Ú]+$/g.test(this.UsersObject.Apellidos)) {
                swal('Error', 'El campo apellido solo admite letras!', 'error');
            }

            else if (this.UsersObject.Documentos == '') {
                swal('Error', 'Ingrese el documento!', 'error');
            }
            else if (this.UsersObject.Cargo == '') {
                swal('Error', 'Ingrese el cargo!', 'error');
            }
            else if (this.UsersObject.CorreoElectronico == '') {
                swal('Error', 'Ingrese el correo electronico!', 'error');
            }

            else if (!/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(this.UsersObject.CorreoElectronico)) {
                swal('Error', 'El correo ingresado no es valido!', 'error');

            }
            else if (this.UsersObject.Password == '') {
                swal('Error', 'Ingrese la contraseña!', 'error');

            }
            else if (this.UsersObject.ConfirmPassword != this.UsersObject.Password) {

                swal('Error', 'Las contraseñas no coinciden!', 'error');
            }


            else if (this.UsersObject.IdRol == '') {
                swal('Error', 'Ingrese los rol!', 'error');
            }


            else {


                this._UsersService.UpadateUsers(this.DatosServidorObject.Url, this.UsersObject).subscribe(
                    data => {

                        this._AuditoriaService.GuardarAuditoria(this.DatosServidorObject.Url, this.dataUsuario, "Modificar", "Modificar Usuario").subscribe(
                            data => {
                            },
                            error => {
                                alert('ERROR: -----------> ' + error);
                            },
                            () => {
                            }
                        );
                        swal('Correcto', 'Modificación satisfactoria!', 'success');
                        this.SearchUsers();
                    },
                    error => {
                        alert('ERROR: -----------> ' + error);
                    },
                    () => {

                    }
                );

            }

        } catch (error) {

        }

    }


    SearchUsers() {

        try {

            this._UsersService.SearchUsers(this.DatosServidorObject.Url).subscribe(
                (data) => {

                    this.DataUsers = data.value;
                },
                error => alert(error),
            );

        } catch (error) {

        }

    }



    CargarUsuario(datos) {

        this.UsersObject = new Users (datos.Nombre,datos.Apellido,datos.Documento, datos.Cargo, datos.CorreoElectronico, datos.Password, datos.IdRol, '', datos.IdUsuario);

        this.BotonActualizar = true;
    }

    Cancelar() {

        this.BotonActualizar = false;

        this.UsersObject = new Users('', '', '', '', '', '', '', '');


    }


    BuscarUsuario() {

        try {

            this._AuditoriaService.BuscarUsuario(this.DatosServidorObject.Url, 1).subscribe(
                (data) => {

                    this.dataUsuario = data.value[0].Nombre;

                },
                error => alert(error),
            );

        } catch (error) {

        }


    }
}




