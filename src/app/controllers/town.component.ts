import { Component, OnInit,ViewChild } from '@angular/core';
import { Town } from '../models/town';
import { Distrito } from '../models/distrito';
import { Circuito } from '../models/circuito';
import { TipoJuzgado } from '../models/tipojuzgado';
import { Juzgados } from '../models/juzgados';
import { TownService } from '../services/town.service';
import { DistritoService } from '../services/distrito.service';
import { CircuitoService } from '../services/circuito.service';
import { TipoJuzgadoService } from '../services/tipojuzgado.service';
import { JuzgadoService } from '../services/juzgado.service';
import { AuditoriaService } from '../services/auditoria.service';
import { DatosServidor } from '../models/DatosServidor';
import { ModalDirective } from "ngx-bootstrap/modal";


declare var swal: any;


@Component({
    selector: 'Town',
    templateUrl: '../views/town.component.html',
    styleUrls: ['../../assets/css/town-component.css'],
    providers: [TownService, DistritoService, CircuitoService, TipoJuzgadoService, JuzgadoService, AuditoriaService],



})

export class TownComponent implements OnInit {
    @ViewChild('Juzgado') public Juzgado: ModalDirective;

    constructor(private _TownService: TownService, private _DistritoService: DistritoService, private _CircuitoService: CircuitoService, private _TipoJuzgadoService: TipoJuzgadoService, private _JuzgadoService: JuzgadoService, private _AuditoriaService: AuditoriaService) { }


    DatosServidorObject = new DatosServidor();
    MunicipioObject = new Town('', '');
    DistritoObject = new Distrito('');
    CircuitoObject = new Circuito('', '');
    TipoJuzgadoObject = new TipoJuzgado('', '');
    JuzgadosObjecto = new Juzgados('', false, '', '', '');
    datamunucipio = '';
    datacircuitos = '';
    datadistrito = '';
    datacircuitoscon = '';
    datadistritocircuito = '';
    datatipojuzgado=[];
    DataJuzgado;
    datatipojuzgadoSelect = '';
    datacircuitoxdistrito = '';
    datamunicipioxcircuito = '';
    datacircuitoxmunicipio = '';
    BotonActualizar = false;
    Naturaleza = '';
    datanaturaleza = ''
    datavdistrito = '';
    datavcircuito = '';
    datavmunicipio = '';
    dataUsuario = '';

    ngOnInit() {

        // SaveTown();
        this.BuscarMunicipio();
        this.BuscarCircuitoMunicipio();
        this.BuscarDistrito();
        this.BuscarCircuito();
        this.BuscarDistritoCircuito();
        this.BuscarTipoJuzgado();
        // this.BuscarTipoJuzgadoSelect();
        this.BuscarTipoNaturaleza();
        this.BuscarUsuario();
    }



    BuscarMunicipio() {

        try {

            this._TownService.BuscarMunicipio(this.DatosServidorObject.Url).subscribe(
                (data) => {

                    this.datamunucipio = data.value;
                    // console.log(JSON.stringify(data));
                },
                error => alert(error),
            );

        } catch (error) {

        }

    }

    BuscarCircuitoMunicipio() {

        try {

            this._TownService.BuscarCircuitoMunicipio(this.DatosServidorObject.Url).subscribe(
                (data) => {

                    this.datacircuitos = data.value;
                    // console.log(JSON.stringify(data));
                },
                error => alert(error),
            );

        } catch (error) {

        }

    }

    GuardarMunicipio() {

        // try {

        if (this.MunicipioObject.Municipio == '') {
            swal('Error', 'Ingrese el Municipio!', 'error');
        }

        else if (!/^[a-zA-Z ]*$/g.test(this.MunicipioObject.Municipio)) {
            swal('Error', 'el campo municipio solo admite letras!', 'error');
        }

        else if (this.MunicipioObject.IdCircuito == '') {
            swal('Error', 'Ingrese el Circuito!', 'error');
        }


        else {

            this._TownService.BuscarMunicipioExistente(this.DatosServidorObject.Url, this.MunicipioObject.Municipio).subscribe(
                (data) => {

                    // this.datavdistrito = data.value;
                    console.log(JSON.stringify(data));

                    if (data.value.length != 0) {

                        swal('Error', 'Este registro ya existe!', 'error');
                    }


                    else {

                        this._TownService.GuardarMunicipio(this.DatosServidorObject.Url, this.MunicipioObject).subscribe(
                            data => {

                                this._AuditoriaService.GuardarAuditoria(this.DatosServidorObject.Url, this.dataUsuario, "Guardar", "Insertar Municipio").subscribe(
                                    data => {
                                        swal('Correcto', 'Registro satisfactorio!', 'success');
                                        this.BuscarMunicipio();

                                    },
                                    error => {
                                        alert('ERROR: -----------> ' + error);
                                    },
                                    () => {

                                    }

                                );

                            },
                            error => {
                                alert('ERROR: -----------> ' + error);
                            },
                            () => {

                            }

                        );
                    }
                }, error => alert(error),  //  catch (error) {
            );
        }
    }



    CargarMunicipio(d) {

        console.log(JSON.stringify(d));

        this.MunicipioObject = new Town(d.Nombre, d.IdCircuito, d.IdMunicipio);
        this.BotonActualizar = true;
    }




    ActualizarMunicipio() {

        try {

            if (this.MunicipioObject.Municipio == '') {
                swal('Error', 'Ingrese el municipio!', 'error');
            }

            else if (!/^[a-zA-Z ]*$/g.test(this.MunicipioObject.Municipio)) {
                swal('Error', 'el campo nombre solo admite letras!', 'error');
            }

            else if (this.MunicipioObject.IdCircuito == '') {
                swal('Error', 'Ingrese el circuito!', 'error');
            }


            else {

                this._TownService.BuscarMunicipioExistenteActualizar(this.DatosServidorObject.Url, this.MunicipioObject.Municipio, this.MunicipioObject.IdMunicipio).subscribe(
                    (data) => {
                        console.log(JSON.stringify(data));


                        if (data.value.length != 0) {

                            swal('Error', 'Este registro ya existe!', 'error');
                        }

                        else {


                            this._TownService.ActualizarMunicipio(this.DatosServidorObject.Url, this.MunicipioObject).subscribe(
                                data => {
                                    this._AuditoriaService.GuardarAuditoria(this.DatosServidorObject.Url, this.dataUsuario, "Modificar", "Actualizar Municipio").subscribe(
                                        data => {
                                            swal('Correcto', 'Modificación satisfactoria!', 'success');
                                            this.BuscarMunicipio();
                                            this.MunicipioObject = new Town('', '');


                                        },
                                        error => {
                                            alert('ERROR: -----------> ' + error);
                                        },
                                        () => {

                                        }
                                    );
                                },
                                error => {
                                    alert('ERROR: -----------> ' + error);
                                },
                                () => {

                                }
                            );

                        }

                    }, error => alert(error),  //  catch (error) {
                );
            }





        } catch (error) {

        }

    }

    cancelar() {

        this.BotonActualizar = false;
        this.MunicipioObject = new Town('', '');
    }





    //   METODOS PARA LA VISTA DE DISTRITOS///////////////////////////////////////////////////////////////


    BuscarDistrito() {

        try {

            this._DistritoService.BuscarDistrito(this.DatosServidorObject.Url).subscribe(
                (data) => {

                    this.datadistrito = data.value;
                    // console.log(JSON.stringify(data));
                },
                error => alert(error),
            );

        } catch (error) {

        }

    }




    GuardarDistrito() {

        try {

            if (this.DistritoObject.Nombre == '') {
                swal('Error', 'Ingrese el Distrito!', 'error');
            }

            else if (!/^[a-zA-Z . á é í ó ú]*$/g.test(this.DistritoObject.Nombre)) {
                swal('Error', 'el campo distrito solo admite letras!', 'error');
            }

            else {

                this._DistritoService.BuscarDistritoExistente(this.DatosServidorObject.Url, this.DistritoObject.Nombre).subscribe(
                    (data) => {

                        if (data.value.length != 0) {

                            swal('Error', 'Este registro ya existe!', 'error');


                        }

                        else {

                            this._DistritoService.GuardarDistrito(this.DatosServidorObject.Url, this.DistritoObject).subscribe(
                                data => {
                                    this._AuditoriaService.GuardarAuditoria(this.DatosServidorObject.Url, this.dataUsuario, "Guardar", "Insertar Distrito").subscribe(
                                        data => {
                                            swal('Correcto', 'Registro satisfactorio!', 'success');
                                            this.BuscarDistrito();

                                        },
                                        error => {
                                            alert('ERROR: -----------> ' + error);
                                        },
                                        () => {

                                        }
                                    );

                                },
                                error => {
                                    alert('ERROR: -----------> ' + error);
                                },
                                () => {

                                }
                            );

                        }
                    },
                    error => alert(error),
                );


            }
        } catch (error) {

        }

    }

    
    CargarDistrito(datos) {

        
        this.DistritoObject = new Distrito(datos.Nombre, datos.IdDistrito);
        this.BotonActualizar = true;
    }

    // CargarDistrito(Nombre, IdDistrito) {

    //     this.DistritoObject = new Distrito(Nombre, IdDistrito);
    //     this.BotonActualizar = true;
    // }



    ActualizarDistrito() {

        try {

            if (this.DistritoObject.Nombre == '') {
                swal('Error', 'Ingrese el Distrito!', 'error');
            }

            else if (!/^[a-zA-Z . á é í ó ú]*$/g.test(this.DistritoObject.Nombre)) {
                swal('Error', 'el campo distrito solo admite letras!', 'error');
            }

            else {

                this._DistritoService.BuscarDistritoExistenteActualizar(this.DatosServidorObject.Url, this.DistritoObject.Nombre, this.DistritoObject.IdDistrito).subscribe(
                    (data) => {
                        console.log(JSON.stringify(data));
                        if (data.value.length != 0) {

                            swal('Error', 'Este registro ya existe!', 'error');
                        }

                        else {

                            this._DistritoService.ActualizarDistrito(this.DatosServidorObject.Url, this.DistritoObject).subscribe(
                                data => {
                                    this._AuditoriaService.GuardarAuditoria(this.DatosServidorObject.Url, this.dataUsuario, "Modificar", "Actualizar Distrito").subscribe(
                                        data => {
                                            swal('Correcto', 'Modificación satisfactoria!', 'success');
                                            this.BuscarDistrito();
                                            this.DistritoObject = new Distrito('');

                                        },
                                        error => {
                                            alert('ERROR: -----------> ' + error);
                                        },
                                        () => {

                                        }
                                    );

                                },
                                error => {
                                    alert('ERROR: -----------> ' + error);
                                },
                                () => {

                                }
                            );

                        }


                    }, error => alert(error),  //  catch (error) {
                );
            }



        } catch (error) {

        }

    }

    cancelarDistrito() {

        this.BotonActualizar = false;
        this.DistritoObject = new Distrito('');
    }

    // TODOS LOS METODOS PARA CIRCUITOS///////////////////////////////////////////////////////////////////////


    BuscarCircuito() {

        try {

            this._CircuitoService.BuscarCircuito(this.DatosServidorObject.Url).subscribe(
                (data) => {

                    this.datacircuitoscon = data.value;
                    // console.log(JSON.stringify(data));
                },
                error => alert(error),
            );

        } catch (error) {

        }

    }


    BuscarDistritoCircuito() {

        try {

            this._CircuitoService.BuscarDistritoCircuito(this.DatosServidorObject.Url).subscribe(
                (data) => {

                    this.datadistritocircuito = data.value;
                    // console.log(JSON.stringify(data));
                },
                error => alert(error),
            );

        } catch (error) {

        }

    }


    GuardarCircuito() {

        try {

            if (this.CircuitoObject.Nombre == '') {
                swal('Error', 'Ingrese el circuito!', 'error');
            }

            else if (!/^[a-zA-Z . á é í´o ú]*$/g.test(this.CircuitoObject.Nombre)) {
                swal('Error', 'el campo circuito solo admite letras!', 'error');
            }

            else if (this.CircuitoObject.IdDistrito == '') {
                swal('Error', 'Ingrese el Distrito!', 'error');
            }

            else {

                this._CircuitoService.BuscarCircuitoExistente(this.DatosServidorObject.Url, this.CircuitoObject.Nombre).subscribe(
                    (data) => {

                        // this.datavdistrito = data.value;
                        console.log(JSON.stringify(data));

                        if (data.value.length != 0) {

                            swal('Error', 'Este registro ya existe!', 'error');
                        }

                        else {


                            this._CircuitoService.GuardarCircuito(this.DatosServidorObject.Url, this.CircuitoObject).subscribe(
                                data => {
                                    this._AuditoriaService.GuardarAuditoria(this.DatosServidorObject.Url, this.dataUsuario, "Guardar", "Insertar Circuito").subscribe(
                                        data => {
                                            swal('Correcto', 'Registro satisfactorio!', 'success');
                                            this.BuscarCircuito();

                                        },
                                        error => {
                                            alert('ERROR: -----------> ' + error);
                                        },
                                        () => {

                                        }
                                    );

                                },
                                error => {
                                    alert('ERROR: -----------> ' + error);
                                },
                                () => {

                                }
                            );

                        }

                    }, error => alert(error),  //  catch (error) {
                );
            }


        } catch (error) {

        }

    }


    ActualizarCircuito() {

        try {

            if (this.CircuitoObject.Nombre == '') {
                swal('Error', 'Ingrese el circuito!', 'error');
            }

            else if (!/^[a-zA-Z . á é í ó ú]*$/g.test(this.CircuitoObject.Nombre)) {
                swal('Error', 'el campo circuito solo admite letras!', 'error');
            }

            else if (this.CircuitoObject.IdDistrito == '') {
                swal('Error', 'Ingrese el Distrito!', 'error');
            }


            else {

                this._CircuitoService.BuscarCircuitoExistenteActualizar(this.DatosServidorObject.Url, this.CircuitoObject.Nombre, this.CircuitoObject.IdCircuito).subscribe(
                    (data) => {

                        // this.datavdistrito = data.value;
                        console.log(JSON.stringify(data));

                        if (data.value.length != 0) {

                            swal('Error', 'Este registro ya existe!', 'error');
                        }

                        else {


                            this._CircuitoService.ActualizarCircuito(this.DatosServidorObject.Url, this.CircuitoObject).subscribe(
                                data => {
                                    this._AuditoriaService.GuardarAuditoria(this.DatosServidorObject.Url, this.dataUsuario, "Modificar", "Actualizar Circuito").subscribe(
                                        data => {
                                            swal('Correcto', 'Modificación satisfactoria!', 'success');
                                            this.BuscarCircuito();
                                            this.CircuitoObject = new Circuito('', '');

                                        },
                                        error => {
                                            alert('ERROR: -----------> ' + error);
                                        },
                                        () => {

                                        }
                                    );

                                },
                                error => {
                                    alert('ERROR: -----------> ' + error);
                                },
                                () => {

                                }
                            );

                        }


                    }, error => alert(error),  //  catch (error) {
                );
            }



        } catch (error) {

        }

    }

    CargarCircuito(d) {

        this.CircuitoObject = new Circuito(d.Nombre, d.IdDistrito, d.IdCircuito);
        this.BotonActualizar = true;
    }



    cancelarCircuito() {

        this.BotonActualizar = false;
        this.CircuitoObject = new Circuito('', '');
    }




    // TIPO JUZGADO///////////////////////////


    BuscarTipoJuzgado() {

        try {

            this._JuzgadoService.BuscarNaturaleza(this.DatosServidorObject.Url).subscribe(
                (data) => {

                    console.log(JSON.stringify(data));
                    this.datatipojuzgado = [];

                    
                    for (let item of data.value) {

                        var ConJuzgado=0;
                        var NumJuzgado=data.value.length;

                        this._JuzgadoService.BuscarJuzgadoxNaturaleza(this.DatosServidorObject.Url, item.IdNaturaleza).subscribe(
                            (data) => {

                                ConJuzgado++;

                                for (let itemJuzgado of data.value) {
                                    var jsonEntrada = { IdNaturaleza: item.IdNaturaleza, Nombre: item.Nombre, IdJuzgado: itemJuzgado.IdJuzgado, Juzgado1: itemJuzgado.Juzgado1, Juez: itemJuzgado.Juez, IdMunicipio: itemJuzgado.IdMunicipio, Circuito: itemJuzgado.Circuito };

                                    this.datatipojuzgado.push(jsonEntrada);
                                }
                                if(ConJuzgado==NumJuzgado){
                                    this.DataJuzgado=this.datatipojuzgado;
                                }
                            }
                        )


                    }
                    console.log(JSON.stringify(this.datatipojuzgado));
                },
                error => alert(error),
            );

        } catch (error) {

        }

    }

    ConsultarCircuitoXDistrito(IdDistrito) {

        try {

            this._CircuitoService.BuscarCircuitoXDistrito(this.DatosServidorObject.Url, IdDistrito).subscribe(
                (data) => {

                    this.datacircuitoxdistrito = data.value;

                    // console.log(JSON.stringify(data));
                },
                error => alert(error),
            );

        } catch (error) {

        }
    }

    ConsultarMunicipioXCircuito(IdCircuito) {

        try {

            this._TownService.BuscarMunicipioXCircuito(this.DatosServidorObject.Url, IdCircuito).subscribe(
                (data) => {

                    this.datamunicipioxcircuito = data.value;

                    // console.log(JSON.stringify(data));
                },
                error => alert(error),
            );

        } catch (error) {

        }
    }

    // GUARDAR JUZGADO


    GuardarJuzgado(DistritoSelect, Circuito) {

        // console.log(Distrito);

        try {

            if (DistritoSelect == "") {
                swal('Error', 'Seleccione el Distrito!', 'error');
            }

            else if (Circuito == "") {
                swal('Error', 'Seleccione circuito!', 'error');

            }

            else if (this.JuzgadosObjecto.IdNaturaleza == '') {
                swal('Error', 'Ingrese la naturaleza!', 'error');
            }

            else if (this.JuzgadosObjecto.IdMunicipio == '') {
                swal('Error', 'Ingrese el municipio!', 'error');
            }

            else if (this.JuzgadosObjecto.Descripcion == '') {
                swal('Error', 'Ingrese el nombre!', 'error');
            }


            else if (!/^[a-zA-Z . á é í ó ú]*$/g.test(this.JuzgadosObjecto.Descripcion)) {
                swal('Error', 'el campo nombre solo admite letras!', 'error');
            }

            else if (this.JuzgadosObjecto.Juez == '') {
                swal('Error', 'Ingrese el Juez!', 'error');
            }


            else if (!/^[a-zA-Z . á é í ó ú ]*$/g.test(this.JuzgadosObjecto.Juez)) {
                swal('Error', 'el campo juez solo admite letras!', 'error');
            }

            else {

                // console.log(this.JuzgadosObjecto.IdTipoJuzgado,this.JuzgadosObjecto.IdMunicipio,this.JuzgadosObjecto.Circuito,this.JuzgadosObjecto.Descripcion,this.JuzgadosObjecto.Juez)

                this._JuzgadoService.GuardarJuzgados(this.DatosServidorObject.Url, this.JuzgadosObjecto).subscribe(
                    data => {
                        this._AuditoriaService.GuardarAuditoria(this.DatosServidorObject.Url, this.dataUsuario, "Guardar", "Insertar Juzgado").subscribe(
                            data => {
                                swal('Correcto', 'Registro satisfactorio!', 'success');
                                this.BuscarTipoJuzgado();

                            },
                            error => {
                                alert('ERROR: -----------> ' + error);
                            },
                            () => {

                            }
                        );

                    },
                    error => {
                        alert('ERROR: -----------> ' + error);
                    },
                    () => {

                    }
                );

            }

        } catch (error) {

        }

    }

    LlevarDatosJuzgado(d) {





        try {
            // console.log(JSON.stringify(d));

            // this.ConsultarCircuitoXDistrito;
            // this.JuzgadosObjecto = new Juzgados(d.IdMunicipio, d.Circuito, d.Juzgado1, d.Juez, d.IdNaturaleza, d.IdJuzgado);
            this.JuzgadosObjecto = new Juzgados(d.IdMunicipio, d.Circuito, d.Juzgado1, d.Juez, d.IdNaturaleza, d.IdJuzgado);
            
            this.BotonActualizar = true;
            // this.BuscarNaturaleza(IdTipoJuzgado + '|' + Naturaleza);

            this._TownService.BuscarCircuitoXMunicipio(this.DatosServidorObject.Url, d.IdMunicipio).subscribe(
                (data) => {

                    var datacircuitoxmunicipio = data.value;

                    this.MunicipioObject.IdCircuito = datacircuitoxmunicipio[0].IdCircuito;

                    this._TownService.BuscarMunicipioXCircuito(this.DatosServidorObject.Url, datacircuitoxmunicipio[0].IdCircuito).subscribe(
                        (data) => {

                            this.datamunicipioxcircuito = data.value;

                            this.JuzgadosObjecto.IdMunicipio = d.IdMunicipio;
                            // console.log(JSON.stringify(data));



                        },
                        error => alert(error),
                    );


                    this._CircuitoService.BuscarDistritoXCircuito(this.DatosServidorObject.Url, datacircuitoxmunicipio[0].IdCircuito).subscribe(
                        (data) => {

                            this.CircuitoObject.IdDistrito = data.value[0].IdDistrito;

                            this._CircuitoService.BuscarCircuitoXDistrito(this.DatosServidorObject.Url, data.value[0].IdDistrito).subscribe(
                                (data) => {

                                    this.datacircuitoxdistrito = data.value;
                                    this.Juzgado.show();



                                },
                                error => alert(error),
                            )


                        },
                        error => alert(error),
                    );


                },
                error => alert(error),
            );

        } catch (error) {

        }


    }





    ActualizarJuzgado() {

        // console.log(Distrito);

        try {


            if (this.JuzgadosObjecto.IdNaturaleza == '') {
                swal('Error', 'Ingrese la naturaleza!', 'error');
            }

            else if (this.JuzgadosObjecto.IdMunicipio == '') {
                swal('Error', 'Ingrese el municipio!', 'error');
            }


            else if (this.JuzgadosObjecto.Descripcion == '') {
                swal('Error', 'Ingrese el nombre!', 'error');
            }


            else if (!/^[a-zA-Z . á é í ó ú]*$/g.test(this.JuzgadosObjecto.Descripcion)) {
                swal('Error', 'el campo nombre solo admite letras!', 'error');
            }

            else if (this.JuzgadosObjecto.Juez == '') {
                swal('Error', 'Ingrese el Juez!', 'error');
            }


            else if (!/^[a-zA-Z ]*$/g.test(this.JuzgadosObjecto.Juez)) {
                swal('Error', 'el campo juez solo admite letras!', 'error');
            }

            else {

                // console.log(this.JuzgadosObjecto.IdTipoJuzgado,this.JuzgadosObjecto.IdMunicipio,this.JuzgadosObjecto.Circuito,this.JuzgadosObjecto.Descripcion,this.JuzgadosObjecto.Juez)

                this._JuzgadoService.ActualizarJuzgados(this.DatosServidorObject.Url, this.JuzgadosObjecto).subscribe(
                    data => {
                        this._AuditoriaService.GuardarAuditoria(this.DatosServidorObject.Url, this.dataUsuario, "Modificar", "Actualzar Juzgado").subscribe(
                            data => {
                                swal('Correcto', 'Modificaciòn satisfactoria!', 'success');
                                this.BuscarTipoJuzgado();
                                this.JuzgadosObjecto = new Juzgados('', false, '', '', '');

                            },
                            error => {
                                alert('ERROR: -----------> ' + error);
                            },
                            () => {

                            }
                        );

                    },
                    error => {
                        alert('ERROR: -----------> ' + error);
                    },
                    () => {

                    }
                );

            }

        } catch (error) {

        }

    }

    cancelarJuzgado() {

        this.BotonActualizar = false;
        this.JuzgadosObjecto = new Juzgados('', false, '', '', '');
    }



    BuscarTipoNaturaleza() {

        try {

            this._JuzgadoService.BuscarNaturaleza(this.DatosServidorObject.Url).subscribe(
                (data) => {

                    this.datanaturaleza = data.value;
                    // console.log(JSON.stringify(data));
                },
                error => alert(error),
            );

        } catch (error) {

        }

    }

    BuscarUsuario() {

        try {

            this._AuditoriaService.BuscarUsuario(this.DatosServidorObject.Url, 1).subscribe(
                (data) => {

                    this.dataUsuario = data.value[0].Nombre;

                },
                error => alert(error),
            );

        } catch (error) {

        }


    }


}