import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Register } from '../models/register';
import { Historial } from '../models/historial';
import { Campos } from '../models/campos';
import { Pendientes } from '../models/Pendientes';
import { RegisterService } from '../services/register.service';
import { HistorialService } from '../services/historial.service';
import { CamposFormularioService } from '../services/camposFormulario.service';
import { EstadosTipoFormularioService } from '../services/EstadoTipoFormulario.service';
import { ChequeoFormularioService } from '../services/chequeoFormulario.service';
import { JuzgadoService } from '../services/juzgado.service';
import { SalarioService } from '../services/salario.service';
import { PdteServices } from '../services/EstadosFormulario.service';
import { ProyectoService } from '../services/proyecto.service';
import { DatosServidor } from '../models/DatosServidor';
import { PresentacionDemanda } from '../models/presentacionDemanda';
import { CustomerService } from '../services/customer.service';
import { AuditoriaService } from '../services/auditoria.service';
import { UsersService } from '../services/users.service';
import { PresentacionDemandaService } from '../services/presentacionDemanda.service';
// import {ModalDirective} from 'ng2-bootstrap/modal';
import { ModalDirective } from "ngx-bootstrap/modal";

declare var swal: any;

@Component({
    selector: 'register',
    templateUrl: '../views/register.component.html',
    styleUrls: ['../../assets/css/register-component.css'],
    providers: [RegisterService, PresentacionDemandaService, HistorialService, CamposFormularioService, EstadosTipoFormularioService, ChequeoFormularioService, JuzgadoService, SalarioService, ProyectoService, CustomerService, UsersService, PdteServices, AuditoriaService]
})


export class RegisterComponent implements OnInit {
    @ViewChild('modalDocument') public modalDocument: ModalDirective;
    @ViewChild('modalEtapas') public modalEtapas: ModalDirective;
    @ViewChild('JuzgadoCompetente') public JuzgadoCompetente: ModalDirective;
    @ViewChild('Proyectos') public Proyectos: ModalDirective;
    @ViewChild('DemandanteDocumento') public DemandanteDocumento: ModalDirective;
    @ViewChild('ResponsableModal') public ResponsableModal: ModalDirective;
    @ViewChild('ResponsableSucesor') public ResponsableSucesor: ModalDirective;

    constructor(private _RegisterService: RegisterService, private _PresentacionDemandaService: PresentacionDemandaService, private _HistorialService: HistorialService, private _CamposFormularioService: CamposFormularioService, private _EstadosTipoFormularioService: EstadosTipoFormularioService, private _ChequeoFormularioService: ChequeoFormularioService, private router: Router, private _JuzgadoService: JuzgadoService, private _SalarioService: SalarioService, private _ProyectoService: ProyectoService, private _CustomerService: CustomerService, private _UsersService: UsersService, private _PdteServices: PdteServices, private _AuditoriaService: AuditoriaService) {

        var formulario = router.parseUrl(router.url).queryParams["IdFormulario"];
        this.ParametroUrl = formulario;
    }

    ProcessObject = new Register('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
    PresentacionObject = new PresentacionDemanda('', '', '', '', '');
    HistorialObject = new Historial('', '', '', '', '');
    DatosServidorObject = new DatosServidor();
    camposObject = new Campos([], 0);
    PendientesObject = new Pendientes('', '', '', '', '', '');



    ParametroUrl = '';
    DataHistory = [];
    DataPending = [];
    DataDocument = [];
    DataPresentation = [];
    dataPresentacion = '';
    dataTown = '';
    dataCourt = '';
    dataProceso = '';
    datamunicipio = '';
    datajuzgado = '';
    datajuzgadoJuez = '';
    datapersonas = [];
    datatiproceso = '';
    datausuario = '';
    datahistorial = [];
    datacampos = '';
    dataestados = [];
    datachequeo = [];
    datosmodalchequeo = '';
    datacompetente = '';
    ModalNombre = '';
    dataCompetente = '';
    datatipojuzgado = [];
    DataJuzgado;
    dataproyectos = [];
    DataProyectos;
    DataCustomer = '';
    datausuarios = '';
    IdPersona;
    JsonCustomer = {};
    dataPendientes = '';
    IdEstadoFormulario;
    JsonEstadoFormulario = {};
    dataUsuario = '';
    JsonEtapa={};


    ngOnInit() {

        // this.BuscarFormatos();
        // SearchTown();
        // SearchCourt();
        this.BuscarMunicipio();
        this.BuscarPersonas();
        this.BuscarTipoProceso();
        this.BuscarUsuario();
        this.BuscarFormulario();
        this.BuscarTipoJuzgado();
        this.BuscarProyectos();
        this.BuscarPersona();
        this.BuscarUsuarios();
        this.BuscarPersonaUsuario();
        // this.BuscarCampos(this.ProcessObject.IdTiposProcesos);

        console.log(this.ParametroUrl);

    }


    //variable para mostrar un item
    InputVisible = false;


    //Variable que almacenara la configuracion para los select multipleas
    ConfiguracionSelect = {
        pullRight: true,
        pullLeft: true,
        enableSearch: true,
        checkedStyle: 'checkboxes',
        buttonClasses: 'btn btn-default col-md-12',
        selectionLimit: 0,
        closeOnSelect: false,
        showCheckAll: true,
        showUncheckAll: true,
        dynamicTitleMaxItems: 0,
    };

    TextoSelect = {
        checkAll: 'Seleccionar todo',
        uncheckAll: 'Desmarcar todo',
        checked: 'un elemento seleccionado',
        checkedPlural: 'elementos seleccionados',
        searchPlaceholder: 'Buscar',
        defaultTitle: 'Seleccionar opcion...',
        allSelected: 'Todo seleccionado',
    };

    // 

    // BUSCAR LOS MUNICIPIOS

    BuscarMunicipio() {

        try {
            // this.ProcessObject.Juez = '';

            this._RegisterService.BuscarMunicipio(this.DatosServidorObject.Url).subscribe(
                (data) => {

                    this.datamunicipio = data.value;
                    // console.log(JSON.stringify(data));
                },
                error => alert(error),
            );

        } catch (error) {

        }

    }

    BuscarJuzgado(IdMunicipio) {

        try {

            if (IdMunicipio != '') {

                this._RegisterService.BuscarJuzgado(this.DatosServidorObject.Url, IdMunicipio).subscribe(
                    (data) => {

                        this.datajuzgado = data.value;
                        // console.log(JSON.stringify(data));
                    },
                    error => alert(error),
                );
            }


        } catch (error) {

        }

    }

    // buscar personas



    BuscarPersonas() {

        try {

            this._RegisterService.BuscarPersonas(this.DatosServidorObject.Url).subscribe(
                (data) => {

                    this.datapersonas = this.ConstruirOpcionesSelect(data.value);


                },
                error => alert(error),
            );

        } catch (error) {

        }

    }


    ConstruirOpcionesSelect(Datos) {

        var Salida = [];

        for (let item of Datos) {
            Salida.push({ id: item.IdPersona, name: item.Nombre });
        }

        return Salida;

    }



    // BUSCAR TIPO DE PROCESOS

    BuscarTipoProceso() {

        try {


            this._RegisterService.BuscarTipoProceso(this.DatosServidorObject.Url).subscribe(
                (data) => {

                    this.datatiproceso = data.value;
                    // console.log(JSON.stringify(data));
                },
                error => alert(error),
            );

        } catch (error) {

        }

    }





    BuscarUsuario() {

        try {

            this._RegisterService.BuscarUsuario(this.DatosServidorObject.Url).subscribe(
                (data) => {

                    this.datausuario = data.value;
                    // console.log(JSON.stringify(data));
                },
                error => alert(error),
            );

        } catch (error) {

        }

    }


    //     BuscarCampos(IdTiposProcesos) {

    //     console.log(this.ProcessObject.IdTiposProcesos);
    //     try {

    //         this._CamposFormularioService.buscarCamposTipos(this.DatosServidorObject.Url,IdTiposProcesos).subscribe(
    //             (data) => {

    //                 this.datacampos = data.value;
    //                 console.log(JSON.stringify(data));
    //             },
    //             error => alert(error),
    //         );

    //     } catch (error) {

    //     }

    // }

    ConsultarDatosxTipoProceso() {


        // console.log(this.ProcessObject.IdTiposProcesos);
        try {

            this._CamposFormularioService.buscarCamposTipos(this.DatosServidorObject.Url, this.ProcessObject.IdTiposProcesos).subscribe(
                (data) => {

                    this.datacampos = data.value;
                    // console.log(JSON.stringify(data));
                },
                error => alert(error),
            );

        } catch (error) {

        }
    }



    // BUSCAR LOS ESTADOS PROCESALES

    ConsultarEstadosXTipoProceso() {


        // console.log(this.ProcessObject.IdTiposProcesos);
        try {

            this._EstadosTipoFormularioService.buscarEstadosTipos(this.DatosServidorObject.Url, this.ProcessObject.IdTiposProcesos).subscribe(
                (data) => {
                    this.dataestados = [];
                    var jsonData = {};

                    // console.log(JSON.stringify(data.value));

                    for (let item of data.value) {
                        jsonData[item.EstadosProcesales.Nombre] = item;

                    }

                    for (let item of Object.keys(jsonData)) {
                        this.dataestados.push(jsonData[item]);
                    }
                },
                error => alert(error),
            );

        } catch (error) {

        }

    }



    // LISTAS DE CHEQUEO////////////////////////////////////////

    ConsultarChequeosXTipo() {


        // console.log(this.ProcessObject.IdTiposProcesos);
        try {

            this._ChequeoFormularioService.buscarChequeosTipo(this.DatosServidorObject.Url, this.ProcessObject.IdTiposProcesos).subscribe(
                (data) => {
                    this.datachequeo = [];
                    var jsonData = {};

                    // console.log(JSON.stringify(data.value));

                    for (let item of data.value) {
                        jsonData[item.Chequeos.Nombre] = item;
                        console.log(item.Chequeos.Nombre)

                    }

                    for (let item of Object.keys(jsonData)) {
                        this.datachequeo.push(jsonData[item]);
                    }
                },
                error => alert(error),
            );

        } catch (error) {

        }
    }

    CargarDatosCheck( Nombre, IdEstadoFormularioChange) {



       

            this.IdEstadoFormulario = IdEstadoFormularioChange;

            if (Nombre == 'Recibo de documentos') {

                this.modalDocument.show();
                this.ModalNombre = Nombre;
            }

            else {

                this.modalEtapas.show();
                this.ModalNombre = Nombre;
                this.BuscarPdtes();
            }
    }



    // CARGAR INFORMACIÓN EN FORMULARIO PRINCIPAL//////////////////////////////////

    BuscarFormulario() {

        try {
            if (this.ParametroUrl != undefined) {

                this._RegisterService.BuscarFormulario(this.DatosServidorObject.Url, this.ParametroUrl).subscribe(
                    (data) => {

                        // this.ParametroUrl = data.value;
                        // console.log(JSON.stringify(data.value));

                        // this.ProcessObject = new Register(data.value[0].CodProceso, data.value[0].Demandado, data.value[0].Demandante, data.value[0].IdMunicipio, data.value[0].JuzgadoConocimiento, data.value[0].Radicado, data.value[0].Responsable, data.value[0].ResponsableSucesor, data.value[0].IdTIpoProceso, data.value[0].Valor, data.value[0].Cuantia)
                    },
                    error => alert(error),
                );

            }
        } catch (error) {

        }



    }


    AlgoritmoCuantia(valor) {


        this._SalarioService.AlgoritoCuantia(this.DatosServidorObject.Url, valor).subscribe(
            (data) => {
                this.ProcessObject.Cuantia = data;

                if (this.ProcessObject.Municipio != '') {

                    this._JuzgadoService.AlgoritmoJuzgadoCompetente(this.DatosServidorObject.Url, this.ProcessObject).subscribe(
                        data => {

                            this.datacompetente = data;
                        },
                        error => {
                            alert('ERROR: -----------> ' + error);
                        },
                        () => {

                        }
                    );
                }

            },
            (error) => {
                alert('ERROR: -----------> ' + error);
            },
            () => {

            }
        );
    }



    MunicipioCuantia() {

        if (this.ProcessObject.Cuantia != '') {

            this._JuzgadoService.AlgoritmoJuzgadoCompetente(this.DatosServidorObject.Url, this.ProcessObject).subscribe(
                data => {
                    this.datacompetente = data;

                    console.log(data);
                },
                error => {
                    alert('ERROR: -----------> ' + error);
                },
                () => {

                }
            );
        }
    }


    CargarJuzgado(dato) {

        this.ProcessObject.Juez = dato.Juez;
        this.ProcessObject.JuzgadoConocimiento = dato.Juzgado;
        this.JuzgadoCompetente.hide();

    }



    BuscarTipoJuzgado() {

        try {

            this._JuzgadoService.BuscarNaturaleza(this.DatosServidorObject.Url).subscribe(
                (data) => {

                    // console.log(JSON.stringify(data));
                    this.datatipojuzgado = [];


                    for (let item of data.value) {

                        var ConJuzgado = 0;
                        var NumJuzgado = data.value.length;

                        this._JuzgadoService.BuscarJuzgadoxNaturaleza(this.DatosServidorObject.Url, item.IdNaturaleza).subscribe(
                            (data) => {

                                ConJuzgado++;

                                for (let itemJuzgado of data.value) {
                                    var jsonEntrada = { IdNaturaleza: item.IdNaturaleza, Nombre: item.Nombre, IdJuzgado: itemJuzgado.IdJuzgado, Juzgado1: itemJuzgado.Juzgado1, Juez: itemJuzgado.Juez, IdMunicipio: itemJuzgado.IdMunicipio, Circuito: itemJuzgado.Circuito };

                                    this.datatipojuzgado.push(jsonEntrada);
                                }
                                if (ConJuzgado == NumJuzgado) {
                                    this.DataJuzgado = this.datatipojuzgado;
                                }
                            }
                        )


                    }
                    // console.log(JSON.stringify(this.datatipojuzgado));
                },
                error => alert(error),
            );

        } catch (error) {

        }

    }

    CargarTodosLosJuzgados(dato) {

        this.ProcessObject.Juez = dato.Juez;
        this.ProcessObject.JuzgadoConocimiento = dato.Juzgado1;
        this.JuzgadoCompetente.hide();

    }

    BuscarProyectos() {

        try {

            this._ProyectoService.BuscarProyecto(this.DatosServidorObject.Url).subscribe(
                (data) => {

                    this.dataproyectos = [];
                    //  console.log(JSON.stringify(data.value));


                    for (let itemProyecto of data.value) {

                        var ContadorProyectos = 0;
                        var NumProyectos = data.value.length;


                        // console.log(itemProyecto.IdContratos);

                        this._ProyectoService.BuscarNombreContrato(this.DatosServidorObject.Url, itemProyecto.IdContratos).subscribe(
                            (data) => {

                                ContadorProyectos++;

                                var jsonEntrada = { Contrato: data.value[0].Contrato, NombreProyecto: itemProyecto.NombreProyecto, Interventor: itemProyecto.Interventor, Representador: itemProyecto.Representador, IdProyectos: itemProyecto.IdProyectos, IdContratos: itemProyecto.IdContratos, IdPersona: data.value[0].IdPersona };

                                this.dataproyectos.push(jsonEntrada);

                                //  console.log(JSON.stringify(this.datacontrato));

                                if (ContadorProyectos == NumProyectos) {
                                    this.DataProyectos = this.dataproyectos;
                                }
                            }



                        )


                    }
                },
                error => alert(error),
            );

        } catch (error) {

        }

    }



    CargarProyectos(dato) {

        this.ProcessObject.Proyecto = dato.IdProyectos;
        this.IdPersona = dato.IdPersona;
        this.Proyectos.hide();

    }



    BuscarPersona() {

        try {

            this._CustomerService.BuscarPersona(this.DatosServidorObject.Url).subscribe(
                (data) => {

                    this.DataCustomer = data.value;
                },
                error => alert(error),
            );

        } catch (error) {

        }

    }


    CargarDemandante(dato) {

        var ArrayDemandante = this.ProcessObject.Demandante.split(',');

        if (ArrayDemandante[0] != "") {
            if (!this.JsonCustomer.hasOwnProperty(dato.NumeroIdentidad)) {
                this.ProcessObject.Demandante = this.ProcessObject.Demandante + ',' + dato.Nombre + ' - ' + dato.NumeroIdentidad;
                this.JsonCustomer[dato.NumeroIdentidad] = '';
            }
        }
        else {
            this.ProcessObject.Demandante = this.ProcessObject.Demandante + dato.Nombre + ' - ' + dato.NumeroIdentidad;
            this.JsonCustomer[dato.NumeroIdentidad] = '';
        }

    }

    CargarDemandado(dato) {

        var ArrayDemandante = this.ProcessObject.Demandado.split(',');

        if (ArrayDemandante[0] != "") {
            if (!this.JsonCustomer.hasOwnProperty(dato.NumeroIdentidad)) {
                this.ProcessObject.Demandado = this.ProcessObject.Demandado + ',' + dato.Nombre + ' - ' + dato.NumeroIdentidad;
                this.JsonCustomer[dato.NumeroIdentidad] = '';
            }
        }
        else {
            this.ProcessObject.Demandado = this.ProcessObject.Demandado + dato.Nombre + ' - ' + dato.NumeroIdentidad;
            this.JsonCustomer[dato.NumeroIdentidad] = '';
        }

    }

    LimpiarDemandante() {

        this.JsonCustomer = {};
        this.ProcessObject.Demandante = '';

    }

    BuscarUsuarios() {

        try {

            this._UsersService.SearchUsers(this.DatosServidorObject.Url).subscribe(
                (data) => {

                    this.datausuarios = data.value;
                },
                error => alert(error),
            );

        } catch (error) {

        }

    }


    CargarResponsable(datos) {

        this.ProcessObject.Responsable = datos.IdUsuario;
        this.ResponsableModal.hide();
    }

    CargarResponsableSucesor(datos) {

        this.ProcessObject.ResponsableSucesor = datos.IdUsuario;
        this.ResponsableSucesor.hide();
    }



    GuardarProcesoJuridico() {

        try {

            if (this.ProcessObject.NumeroProceso == '') {
                swal('Error', 'ingrese el Numero del proceso', 'error');
            }

            if (!/^[0-9]+$/.test(this.ProcessObject.NumeroProceso)) {
                swal('Error', 'El campo Código de proceso solo admite números', 'error');
            }


            else if (this.ProcessObject.Proyecto == '') {
                swal('Error', 'Ingrese el campo proyecto', 'error');
            }

            else if (this.ProcessObject.Demandante == '') {
                swal('Error', 'Ingrese el campo Demandante', 'error');
            }

            else if (this.ProcessObject.Demandado == '') {
                swal('Error', 'Ingrese el campo Demandado', 'error');
            }

            else if (this.ProcessObject.Radicado == '') {
                swal('Error', 'Ingrese el campo Radicado', 'error');
            }


            else if (!/^[a-zA-Z á é í ó ú Á É Í Ó Ú]+$/g.test(this.ProcessObject.Radicado)) {
                swal('Error', 'El campos Radicado solo admite letras', 'error');
            }

            else if (this.ProcessObject.Valor == '') {
                swal('Error', 'Ingrese el campo Valor', 'error');
            }

            else if (!/^[0-9]+$/.test(this.ProcessObject.Valor)) {
                swal('Error', 'El campo valor solo admite números', 'error');
            }

            else if (this.ProcessObject.Municipio == '') {
                swal('Error', 'Ingrese el campo Municipio', 'error');
            }

            else if (this.ProcessObject.Juez == '') {
                swal('Error', 'Ingrese el campo Juzgado', 'error');
            }

            else if (this.ProcessObject.Estimativo == '') {
                swal('Error', 'Ingrese el campo Estimativo', 'error');
            }

            else if (!/^[0-9]+$/.test(this.ProcessObject.Estimativo)) {
                swal('Error', 'El campo estimativo solo admite letras', 'error');
            }

            else if (this.ProcessObject.IdTiposProcesos == '') {
                swal('Error', 'Ingrese el campo Tipo de proceso', 'error');
            }

            else if (this.ProcessObject.Responsable == '') {
                swal('Error', 'Ingrese el campo Responsable', 'error');
            }

            else if (this.ProcessObject.ResponsableSucesor == '') {
                swal('Error', 'Ingrese el campo Responsable sucesor', 'error');
            }


            else {

                this._RegisterService.GuardarFormularioPrincipal(this.DatosServidorObject.Url, this.ProcessObject, this.IdPersona).subscribe(
                    data => {
                        

                        var ContadorEtapas = 0;

                        console.log(JSON.stringify(this.JsonEtapa));
                        for(let itemEtapas of Object.keys(this.JsonEtapa)){
                            
                            ContadorEtapas++;
                            
                            var ContadorObjetos = 0;
                            for(let itemObjetos of this.JsonEtapa[itemEtapas]){
                                
                                ContadorObjetos++;

                                this._PdteServices.GuardarPendientes(this.DatosServidorObject.Url, itemObjetos, itemEtapas, data.IdFormulario).subscribe(
                                    data => {

                                        if((ContadorEtapas == Object.keys(this.JsonEtapa).length)&&(ContadorObjetos == this.JsonEtapa[itemEtapas].length)){
                                            this._AuditoriaService.GuardarAuditoria(this.DatosServidorObject.Url, this.dataUsuario, "Guardar", "Insertar Proceso").subscribe(
                                                data => {  
                                                    swal('Correcto', 'Registro satisfactorio!', 'success');
                                                    this.BuscarPersonaUsuario();
                                                },
                                                error => {
                                                    alert('ERROR: -----------> ' + error);
                                                },
                                                () => {

                                                }
                                            );
                                        }

                                    },
                                    error => {
                                        alert('ERROR: -----------> ' + error);
                                    },
                                    () => {

                                    }
                                );
                            }
                        }

                    },
                    error => {
                        alert('ERROR: -----------> ' + error);
                    },
                    () => {

                    }
                );

            }

        } catch (error) {

        }

    }


    BuscarPdtes() {

        try {

            this._PdteServices.PendientesFormulario(this.DatosServidorObject.Url, this.IdEstadoFormulario).subscribe(
                (data) => {

                    this.dataPendientes = data.value;
                },
                error => alert(error),
            );

        } catch (error) {

        }

    }


    GuardarPendientes() {

        try {
            if(!this.JsonEtapa.hasOwnProperty(this.IdEstadoFormulario)){
                this.JsonEtapa[this.IdEstadoFormulario] = [];
                this.JsonEtapa[this.IdEstadoFormulario].push(this.PendientesObject);
            }
            else{
                this.JsonEtapa[this.IdEstadoFormulario].push(this.PendientesObject);
            }

            this.PendientesObject = new Pendientes('','','','','','');
        }

        catch (error) {

        }
    }

    BuscarPersonaUsuario() {

        try {

            this._AuditoriaService.BuscarUsuario(this.DatosServidorObject.Url, 1).subscribe(
                (data) => {

                    this.dataUsuario = data.value[0].Nombre;

                },
                error => alert(error),
            );

        } catch (error) {

        }


    }




}
