import { Component, OnInit } from '@angular/core';
import { DatosServidor } from '../models/DatosServidor';
import { ActividadesProximasService } from "../services/actividadesproximas.services";



@Component({
    selector: 'reportregister',
    templateUrl: '../views/reportregister.component.html',
    styleUrls: ['../../assets/css/reportregister-component.css'],
    providers: [ActividadesProximasService]

})

export class ReportregisterComponent implements OnInit {

constructor(private _ActividadesProximasService: ActividadesProximasService){}

ngOnInit(){


this.BuscarActividadesProximas();
}
    DatosServidorObject = new DatosServidor();
    dataActividades='';



    
    BuscarActividadesProximas() {

        try {

            this._ActividadesProximasService.ReporteXUsuario(this.DatosServidorObject.Url).subscribe(
                (data) => {

                    this.dataActividades = data;
                    console.log(JSON.stringify(data));
                },
                error => alert(error),
            );

        } catch (error) {

        }

    }


}