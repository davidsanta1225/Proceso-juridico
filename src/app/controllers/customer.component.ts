import { Component, OnInit } from '@angular/core';
import { Customer } from '../models/customer';
import { CustomerService } from '../services/customer.service';
import { DatosServidor } from '../models/DatosServidor';
import { AuditoriaService } from '../services/auditoria.service';


declare var swal: any;


@Component({
    selector: 'Customer',
    templateUrl: '../views/customer.component.html',
    styleUrls: ['../../assets/css/customer-component.css'],
    providers: [CustomerService, AuditoriaService]

})

export class CustomerComponent implements OnInit {

    constructor(private _CustomerService: CustomerService, private _AuditoriaService: AuditoriaService) { }

    PersonasObject = new Customer('', '', '', '');
    DatosServidorObject = new DatosServidor();
    BotonActualizar = false;

    DataCustomer = '';
    dataUsuario = '';

    ngOnInit() {

        this.BuscarPersona();
        this.BuscarUsuario();

    }




    GuardarPersonas() {

        try {

            if (this.PersonasObject.Nombre == '') {
                swal('Error', 'Ingrese el nombre!', 'error');
            }

            else if (!/^[a-zA-Z á é í ó ú Á É Í Ó Ú]+$/g.test(this.PersonasObject.Nombre)) {
                swal('Error', 'el campo nombre solo admite letras!', 'error');
            }

            else if (this.PersonasObject.NumeroIdentidad == '') {
                swal('Error', 'Ingrese el Numero de identidad!', 'error');
            }

            else if (this.PersonasObject.Correo == '') {
                swal('Error', 'Ingrese el Correo!', 'error');
            }

            else if (!/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(this.PersonasObject.Correo)) {
                swal('Error', 'El correo ingresado no es valido!', 'error');

            }

            else if (this.PersonasObject.Direccion == '') {

                swal('Error', 'Ingrese la dirección', 'error');
            }

            else {


                this._CustomerService.GuardarPersonas(this.DatosServidorObject.Url, this.PersonasObject).subscribe(
                    data => {


                        this._AuditoriaService.GuardarAuditoria(this.DatosServidorObject.Url, this.dataUsuario, "Guardar", "Insertar Persona").subscribe(
                            data => {
                                swal('Correcto', 'Registro satisfactorio!', 'success');
                                this.BuscarPersona();

                            },
                            error => {
                                alert('ERROR: -----------> ' + error);
                            },
                            () => {

                            }
                        );
                    },
                    error => {
                        alert('ERROR: -----------> ' + error);
                    },
                    () => {

                    }
                );

            }

        } catch (error) {

        }

    }

    BuscarPersona() {

        try {

            this._CustomerService.BuscarPersona(this.DatosServidorObject.Url).subscribe(
                (data) => {

                    this.DataCustomer = data.value;
                    // console.log(JSON.stringify(data));
                },
                error => alert(error),
            );

        } catch (error) {

        }

    }


    ActualizarPersonas() {

        try {

            if (this.PersonasObject.Nombre == '') {
                swal('Error', 'Ingrese el nombre!', 'error');
            }

            else if (!/^[a-zA-Z á é í ó ú Á É Í Ó Ú]+$/g.test(this.PersonasObject.Nombre)) {
                swal('Error', 'el campo nombre solo admite letras!', 'error');
            }

            else if (this.PersonasObject.NumeroIdentidad == '') {
                swal('Error', 'Ingrese el Numero de identidad!', 'error');
            }

            else if (this.PersonasObject.Correo == '') {
                swal('Error', 'Ingrese el contrato!', 'error');
            }

            else if (!/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(this.PersonasObject.Correo)) {
                swal('Error', 'El correo ingresado no es valido!', 'error');

            }

            else if (this.PersonasObject.Direccion == '') {

                swal('Error', 'Ingrese la dirección', 'error');
            }

            else {


                this._CustomerService.ActualizarPersonas(this.DatosServidorObject.Url, this.PersonasObject).subscribe(
                    data => {

                        this._AuditoriaService.GuardarAuditoria(this.DatosServidorObject.Url, this.dataUsuario, "Modificar", "Modificar Persona").subscribe(
                            data => {
                                swal('Correcto', 'Modificación satisfactoria!', 'success');
                                this.BuscarPersona();

                            },
                            error => {
                                alert('ERROR: -----------> ' + error);
                            },
                            () => {

                            }
                        );

                    },
                    error => {
                        alert('ERROR: -----------> ' + error);
                    },
                    () => {

                    }
                );

            }

        } catch (error) {

        }

    }

    CargarPersona(datos) {

        this.PersonasObject = new Customer(datos.Nombre, datos.NumeroIdentidad, datos.Correo, datos.Direccion, datos.IdPersona);


        this.BotonActualizar = true;
    }    

    // CargarPersona(Nombre, NumeroIdentidad, Correo, Direccion, IdPersona) {

    //     this.PersonasObject = new Customer(Nombre, NumeroIdentidad, Correo, Direccion, IdPersona);

    //     this.BotonActualizar = true;
    // }

    Cancelar() {

        this.BotonActualizar = false;
        this.PersonasObject = new Customer('', '', '', '');
    }


    BuscarUsuario() {

        try {

            this._AuditoriaService.BuscarUsuario(this.DatosServidorObject.Url, 1).subscribe(
                (data) => {

                    this.dataUsuario = data.value[0].Nombre;

                },
                error => alert(error),
            );

        } catch (error) {

        }


    }






}