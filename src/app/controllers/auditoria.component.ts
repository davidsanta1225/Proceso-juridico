import { Component, OnInit } from '@angular/core';
import { SalarioMinimo } from '../models/salario';
import { SalarioService } from '../services/salario.service';
import { AuditoriaService } from '../services/auditoria.service';


import { DatosServidor } from '../models/DatosServidor';

declare var swal: any;

@Component({
    selector: 'Audotoria',
    templateUrl: '../views/auditoria.component.html',
    styleUrls: ['../../assets/css/auditoria-component.css'],
    providers: [SalarioService, AuditoriaService],
})

export class AuditoriaComponent implements OnInit {

    constructor(private _SalarioService: SalarioService, private _AuditoriaService: AuditoriaService) { }

    DatosServidorObject = new DatosServidor();
    dataSalario = '';
    dataAuditoria = '';
    SalarioObject = new SalarioMinimo('', '');
    BotonActualizar = false;


    ngOnInit() {

        this.BuscarSalario();
        this.BuscarAuditoria();

    }



    BuscarSalario() {

        try {

            this._SalarioService.BuscarSalario(this.DatosServidorObject.Url).subscribe(
                (data) => {

                    this.dataSalario = data.value;
                    // console.log(JSON.stringify(data));
                },
                error => alert(error),
            );

        } catch (error) {

        }


    }


    GuardarSalario() {

        try {

            if (this.SalarioObject.Ano == '') {
                swal('Error', 'ingrese el año', 'error');
            }

            else if (!/^[0-9]+$/.test(this.SalarioObject.Ano)) {
                swal('Error', 'El campo Año solo acepta numeros', 'error');
            }

            else if (parseInt(this.SalarioObject.Ano) < 1900) {
                swal('Error', 'El año ingresado es demasiado antiguo', 'error');
            }

            else if (parseInt(this.SalarioObject.Ano) > 4000) {
                swal('Error', 'El año ingresado es muy lejano', 'error');
            }


            else if (this.SalarioObject.Valor == '') {
                swal('Error', 'ingrese el salario', 'error');
            }

            else if (!/^[0-9 .]+$/.test(this.SalarioObject.Valor)) {
                swal('Error', 'El campo salario solo acepta numeros', 'error');
            }

            else {

                // console.log(this.TypeProcessObject.Nombre);

                this._SalarioService.GuardarSalario(this.DatosServidorObject.Url, this.SalarioObject).subscribe(
                    data => {
                        swal('Correcto', 'Registro satisfactorio!', 'success');
                        this.BuscarSalario();
                        this.SalarioObject = new SalarioMinimo('', '');


                    },
                    error => {
                        alert('ERROR: -----------> ' + error);
                    },
                    () => {

                    }
                );

            }

        } catch (error) {

        }

    }

    CancelarSalario() {

        this.SalarioObject = new SalarioMinimo('', '');
        this.BotonActualizar = false;
    }


    CargarDatosSalario(d) {

        this.BotonActualizar = true;
        // console.log(JSON.stringify(d));
        this.SalarioObject = new SalarioMinimo(d.Valor, d.Ano, d.Idsmlv);


    }




    Actualizarsalario() {

        try {

            if (this.SalarioObject.Ano == '') {
                swal('Error', 'ingrese el año', 'error');
            }

            else if (!/^[0-9]+$/.test(this.SalarioObject.Ano)) {
                swal('Error', 'El campo Año solo acepta numeros', 'error');
            }

            else if (parseInt(this.SalarioObject.Ano) < 1900) {
                swal('Error', 'El año ingresado es demasiado antiguo', 'error');
            }

            else if (parseInt(this.SalarioObject.Ano) > 4000) {
                swal('Error', 'El año ingresado es muy lejano', 'error');
            }


            else if (this.SalarioObject.Valor == '') {
                swal('Error', 'ingrese el salario', 'error');
            }

            else if (!/^[0-9]+$/.test(this.SalarioObject.Valor)) {
                swal('Error', 'El campo salario solo acepta numeros', 'error');
            }

            else {

                // console.log(this.TypeProcessObject.Nombre);

                this._SalarioService.ActualizarSalario(this.DatosServidorObject.Url, this.SalarioObject).subscribe(
                    data => {
                        swal('Correcto', 'Actualización satisfactoria!', 'success');
                        this.BuscarSalario();
                        this.SalarioObject = new SalarioMinimo('', '');


                    },
                    error => {
                        alert('ERROR: -----------> ' + error);
                    },
                    () => {

                    }
                );

            }

        } catch (error) {

        }

    }


    BuscarAuditoria() {

        try {

            this._AuditoriaService.ReporteDeAuditoria(this.DatosServidorObject.Url).subscribe(
                (data) => {

                    this.dataAuditoria = data.value;
                    // console.log(JSON.stringify(data));
                },
                error => alert(error),
            );

        } catch (error) {

        }


    }








}