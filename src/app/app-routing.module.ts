import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './controllers/home.component';
import { RegisterComponent } from './controllers/register.component';
import { CustomerComponent } from './controllers/customer.component';
import { UsersComponent } from './controllers/users.component';
import { PermissionsComponent } from './controllers/permissions.component';
import { StepsComponent } from './controllers/steps.component';
import { StepsPComponent } from './controllers/stepsp.component';
import { RresponsableComponent } from './controllers/r-responsable.component';
import { LogComponent } from './controllers/log.component';
import { CourtComponent } from './controllers/court.component';
import { MyactivitiesComponent } from './controllers/myactivities.component';
import { ActivitiestateComponent } from './controllers/activitiestate.component';
import { ReportregisterComponent } from './controllers/reportregister.component';
import { TownComponent } from './controllers/town.component';
import { PresentacionComponent } from './controllers/presentacionDemanda.component';
import { DocumentComponent } from './controllers/document.component';
import { TypeProcessComponent } from './controllers/typeProcess.component';
import { AuditoriaComponent } from './controllers/auditoria.component';











const routes: Routes = [
  { path: '', redirectTo: '/register', pathMatch: 'full' },
  { path: 'home',  component: HomeComponent },
  { path: 'register',  component: RegisterComponent },
  { path: 'customer',  component: CustomerComponent },
  { path: 'users',  component: UsersComponent },
  { path: 'permissions',  component: PermissionsComponent },
  { path: 'steps',  component: StepsComponent },
  { path: 'stepsp',  component: StepsPComponent },
  { path: 'r-responsable',  component: RresponsableComponent },
  { path: 'log',  component: LogComponent },
  { path: 'court',  component: CourtComponent },
  { path: 'myactivities',  component: MyactivitiesComponent },
  { path: 'activitiestate',  component: ActivitiestateComponent },
  { path: 'reportregister',  component: ReportregisterComponent },
  { path: 'town',  component: TownComponent },
  { path: 'presentacion',  component: PresentacionComponent },
  { path: 'document',  component: DocumentComponent },
  { path: 'typeprocess',  component: TypeProcessComponent },
  { path: 'auditoria',  component: AuditoriaComponent },
  

  
  
  
  
  
  
  
];

@NgModule({
  imports: [ RouterModule.forRoot(routes, {useHash: true}) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
