import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Headers } from '@angular/http';
import { RequestOptions } from '@angular/http';

import 'rxjs/add/operator/map';

@Injectable()
export class CamposFormularioService {
    constructor(private _http: Http) { }


    buscarCamposTipos(UrlApi, IdTiposProcesos) {
        var params = '';
        return this._http.get(UrlApi + 'odata/CamposTipos?$select=*,CamposAdicionales/Nombre&$expand=CamposAdicionales&$filter=IdTiposProcesos eq ' + IdTiposProcesos).map(res => res.json());
    }

}