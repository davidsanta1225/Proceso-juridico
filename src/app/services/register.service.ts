import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Headers } from '@angular/http';
import { RequestOptions } from '@angular/http';


import 'rxjs/add/operator/map';

@Injectable()
export class RegisterService {
    constructor(private _http: Http) { }


    BuscarMunicipio(UrlApi) {
        return this._http.get(UrlApi + 'odata/Municipios').map(res => res.json());
    }


    BuscarJuzgado(UrlApi, IdMunicipio) {
        return this._http.get(UrlApi + 'odata/Juzgadoes?$filter=IdMunicipio eq ' + IdMunicipio).map(res => res.json());
    }

    BuscarJuzgadoJuez(UrlApi, IdJuzgado) {
        return this._http.get(UrlApi + 'odata/Juzgadoes?$filter=IdJuzgado eq ' + IdJuzgado).map(res => res.json());
    }

    BuscarPersonas(UrlApi) {
        return this._http.get(UrlApi + 'odata/Personas').map(res => res.json());
    }


    BuscarTipoProceso(UrlApi) {
        return this._http.get(UrlApi + 'odata/TipoProcesos').map(res => res.json());
    }

    BuscarUsuario(UrlApi) {
        return this._http.get(UrlApi + 'odata/Usuarios').map(res => res.json());
    }


    BuscarFormulario(UrlApi, IdFormulario) {
        return this._http.get(UrlApi + 'odata/Formularios?$filter=IdFormulario eq ' + IdFormulario).map(res => res.json());
    }

    GuardarFormularioPrincipal(UrlServicio, ProcessObject,IdPersona) {


        var Datos = { 
            CodProceso: ProcessObject.NumeroProceso, 
            IdProyecto: ProcessObject.Proyecto,
            IdPersona: IdPersona,
            Demandante: ProcessObject.Demandante, 
            Demandado: ProcessObject.Demandado,
            JuzgadoConocimiento: ProcessObject.JuzgadoConocimiento,
            Juez: ProcessObject.Juez,
            IdMunicipio:ProcessObject.Municipio,
            Estimativo: ProcessObject.Estimativo,
            IdTIpoProceso: ProcessObject.IdTiposProcesos,
            Responsable: ProcessObject.Responsable,
            Radicado: ProcessObject.Radicado,
            ResponsableSucesor:ProcessObject.ResponsableSucesor,
            Cuantia: ProcessObject.Cuantia,
            Avaluo: ProcessObject.Valor 
        };

        var headers = new Headers();

        return this._http.post(UrlServicio + 'odata/Formularios', Datos).map(res => res.json());
    }



}