import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Headers } from '@angular/http';
import { RequestOptions } from '@angular/http';

import 'rxjs/add/operator/map';

@Injectable()
export class CamposService {
    constructor(private _http: Http) { }


    SaveCampos(UrlServicio, IdTiposProcesos, IdCampo) {


        var Datos = { IdTiposProcesos: IdTiposProcesos, IdCampos: IdCampo };

        var headers = new Headers();

        return this._http.post(UrlServicio + 'odata/CamposTipos', Datos).map(res => res.json());
    }

    BorrarCamposTipo(UrlServicio, IdCamposTipos) {

        return this._http.delete(UrlServicio + 'odata/CamposTipos(' + IdCamposTipos + ')').map(res => res.json());

    }



    // buscar

    //     SearchCampos(UrlApi){
    //     var params='?Campos';
    //     return this._http.get(UrlApi + '' + params).map(res => res.json());
    // }

    SearchCampos(UrlApi) {
        var params = '';
        return this._http.get(UrlApi + 'odata/CamposAdicionales' + params).map(res => res.json());
    }

    // buscar estados por tipo proceso

    buscarCamposTipos(UrlApi, IdTiposProcesos) {
        var params = '';
        return this._http.get(UrlApi + 'odata/CamposTipos?$filter=IdTiposProcesos eq ' + IdTiposProcesos).map(res => res.json());
    }
}
