import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Headers } from '@angular/http';
import { RequestOptions } from '@angular/http';

import 'rxjs/add/operator/map';

@Injectable()
export class TypeProcessService {
    constructor(private _http: Http) { }


    // Guardar 

    SaveTypeProcess(UrlServicio, TypeProcessObject) {


        var Datos = { Nombre: TypeProcessObject.Nombre };

        var headers = new Headers();

        return this._http.post(UrlServicio + 'odata/TipoProcesos', Datos).map(res => res.json());
    }


    // guardar tipo x estados


    EstadosTipo(UrlServicio, IdTiposProcesos, IdEstados, Prioridad) {

        var Datos = { IdTiposProcesos: IdTiposProcesos, IdEstados: IdEstados, Posicion: Prioridad};

        var headers = new Headers();

        return this._http.post(UrlServicio + 'odata/EstadosTipos', Datos).map(res => res.json());


    }


    BorrarEstadosTipo(UrlServicio, IdEstadosTipos){
        
       return this._http.delete(UrlServicio + 'odata/EstadosTipos('+IdEstadosTipos+')').map(res => res.json());
              
    }


    // buscar

    //     SearchTypeProcess(UrlApi){
    //     var params='?NameProcess=&File=&Activity=&Date';
    //     return this._http.get(UrlApi + '' + params).map(res => res.json());
    // }

    // buscar tipo de proceso

    SearchTypeProcess(UrlApi) {
        var params = '';
        return this._http.get(UrlApi + 'odata/TipoProcesos' + params).map(res => res.json());
    }

    // buscar estados por tipo proceso

    buscarEstadosTipos(UrlApi, IdTiposProcesos) {
        var params = '';
        return this._http.get(UrlApi + 'odata/EstadosTipos?$filter=IdTiposProcesos eq ' + IdTiposProcesos).map(res => res.json());
    }
}