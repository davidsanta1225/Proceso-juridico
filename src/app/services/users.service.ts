import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Headers } from '@angular/http';
import { RequestOptions } from '@angular/http';

import 'rxjs/add/operator/map';                            

@Injectable()
export class UsersService {
    constructor(private _http: Http) { }


    
// Guardar 

    SaveUsers(UrlServicio, UsersObject){


        var Datos = {Nombre: UsersObject.Nombre, Apellido: UsersObject.Apellidos, Documento: UsersObject.Documentos, Cargo: UsersObject.Cargo, CorreoElectronico: UsersObject.CorreoElectronico, Password: UsersObject.Password, IdRol: UsersObject.IdRol};

        console.log(JSON.stringify(Datos));
        
        var headers = new Headers();
            
        return this._http.post(UrlServicio + 'odata/Usuarios', Datos).map(res => res.json());
    }


    UpadateUsers(UrlServicio, UsersObject){


        var Datos = {IdUsuario: UsersObject.IdUsuario, Nombre: UsersObject.Nombre, Apellido: UsersObject.Apellidos, Documento: UsersObject.Documentos, Cargo: UsersObject.Cargo, CorreoElectronico: UsersObject.CorreoElectronico, Password: UsersObject.Password, IdRol: UsersObject.IdRol};
        
        var headers = new Headers();
            
        return this._http.put(UrlServicio + 'odata/Usuarios('+UsersObject.IdUsuario+')', Datos).map(res => res.json());
    }

    

//metodo de consultar

         SearchUsers(UrlApi){
         var params='';

         return this._http.get(UrlApi + 'odata/Usuarios' + params).map(res => res.json());
     }


    //Metodo que elimanara caracteres especiales 

    ValidarCadena(Cadena){

        console.log(Cadena);

        Cadena = Cadena.replace(/'/g, '');
        Cadena = Cadena.replace(/"/g, '');
        Cadena = Cadena.replace(/%/g, '');
        Cadena = Cadena.replace(/&/g, '');
        Cadena = Cadena.replace(/\$/g, '');
        Cadena = Cadena.replace(/!/g, '');
        Cadena = Cadena.replace(/¿/g, '');
        Cadena = Cadena.replace(/\*/g, '');
        Cadena = Cadena.replace(/\?/g, '');
        Cadena = Cadena.replace(/º/g, '');
        Cadena = Cadena.replace(/\//g, '');
        Cadena = Cadena.replace(/\+/g, '');
        
        return Cadena;
    }


}