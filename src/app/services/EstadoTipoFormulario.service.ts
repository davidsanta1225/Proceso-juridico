import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Headers } from '@angular/http';
import { RequestOptions } from '@angular/http';

import 'rxjs/add/operator/map';

@Injectable()
export class EstadosTipoFormularioService {
    constructor(private _http: Http) { }


    buscarEstadosTipos(UrlApi, IdTiposProcesos) {
        var params = '';
        return this._http.get(UrlApi + 'odata/EstadosTipos?$select=*,EstadosProcesales/Nombre,EstadosProcesales/Terminos&$expand=EstadosProcesales&$filter=IdTiposProcesos eq ' + IdTiposProcesos).map(res => res.json());
    }

}