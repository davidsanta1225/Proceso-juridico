import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Headers } from '@angular/http';
import { RequestOptions } from '@angular/http';

import 'rxjs/add/operator/map';                            


@Injectable()
export class CourtService  {
    constructor(private _http: Http) { }




/*
        RegisterCourt(UrlServicio, CourtObject){
        
        var Datos = new FormData;
        Datos.append('Name', CourtObject.Name);
        Datos.append('Town', CourtObject.Town);
        Datos.append('Court', CourtObject.Court);
        
        
        var headers = new Headers();
            
        var options = new RequestOptions({ headers: headers });

        return this._http.post(UrlServicio + 'CourtObject', Datos, options).map(res => res.json());
    }

    //Metodo de consultar

    */

    //     SearchCourt(UrlApi){
    //     var params='?Name=&Name=&Town=&Court';
    //     return this._http.get(UrlApi + '' + params).map(res => res.json());
    // }

    //Metodo que elimanara caracteres especiales 

    ValidarCadena(Cadena){

        console.log(Cadena);

        Cadena = Cadena.replace(/'/g, '');
        Cadena = Cadena.replace(/"/g, '');
        Cadena = Cadena.replace(/%/g, '');
        Cadena = Cadena.replace(/&/g, '');
        Cadena = Cadena.replace(/\$/g, '');
        Cadena = Cadena.replace(/!/g, '');
        Cadena = Cadena.replace(/¿/g, '');
        Cadena = Cadena.replace(/\*/g, '');
        Cadena = Cadena.replace(/\?/g, '');
        Cadena = Cadena.replace(/º/g, '');
        Cadena = Cadena.replace(/\//g, '');
        Cadena = Cadena.replace(/\+/g, '');
        
        return Cadena;
    }

}