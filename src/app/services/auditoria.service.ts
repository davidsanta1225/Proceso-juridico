import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Headers } from '@angular/http';
import { RequestOptions } from '@angular/http';

import 'rxjs/add/operator/map';

@Injectable()
export class AuditoriaService {
    constructor(private _http: Http) { }



    ReporteDeAuditoria(UrlApi) {
        var params = '';
        return this._http.get(UrlApi + 'odata/Auditorias' + params).map(res => res.json());
    }


    BuscarUsuario(UrlApi, IdUsuario) {
        var params = '';
        return this._http.get(UrlApi + 'odata/Usuarios?$filter=IdUsuario eq ' + IdUsuario).map(res => res.json());
    }

    
    GuardarAuditoria(UrlServicio, NombreUsuario,Accion,Descripcion){

        var fecha = new Date();


        var Datos = {NombreUsuario:NombreUsuario, Accion: Accion,  Descripcion: Descripcion,FechaCreacion:fecha};
        
        var headers = new Headers();
            
        return this._http.post(UrlServicio + 'odata/Auditorias', Datos).map(res => res.json());
    }


}