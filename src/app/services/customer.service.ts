import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Headers } from '@angular/http';
import { RequestOptions } from '@angular/http';

import 'rxjs/add/operator/map';                            

@Injectable()
export class CustomerService {
    constructor(private _http: Http) { }


    GuardarPersonas(UrlServicio, PersonasObject){


        var Datos = {Nombre: PersonasObject.Nombre, NumeroIdentidad : PersonasObject.NumeroIdentidad, Correo : PersonasObject.Correo, Direccion: PersonasObject.Direccion};
        
        var headers = new Headers();
            
        return this._http.post(UrlServicio + 'odata/Personas', Datos).map(res => res.json());
    }


//metodo de consultar

         BuscarPersona(UrlApi){
         var params='';

         return this._http.get(UrlApi + 'odata/Personas' + params).map(res => res.json());
     }



    //  actualizar personas

        ActualizarPersonas(UrlServicio, PersonasObject){


        var Datos = {IdPersona: PersonasObject.IdPersona, Nombre: PersonasObject.Nombre, NumeroIdentidad : PersonasObject.NumeroIdentidad ,Correo : PersonasObject.Correo, Direccion: PersonasObject.Direccion};
        
        var headers = new Headers();
            
        return this._http.put(UrlServicio + 'odata/Personas('+PersonasObject.IdPersona+')', Datos).map(res => res.json());
    
}



    //Metodo que elimanara caracteres especiales 

    ValidarCadena(Cadena){

        console.log(Cadena);

        Cadena = Cadena.replace(/'/g, '');
        Cadena = Cadena.replace(/"/g, '');
        Cadena = Cadena.replace(/%/g, '');
        Cadena = Cadena.replace(/&/g, '');
        Cadena = Cadena.replace(/\$/g, '');
        Cadena = Cadena.replace(/!/g, '');
        Cadena = Cadena.replace(/¿/g, '');
        Cadena = Cadena.replace(/\*/g, '');
        Cadena = Cadena.replace(/\?/g, '');
        Cadena = Cadena.replace(/º/g, '');
        Cadena = Cadena.replace(/\//g, '');
        Cadena = Cadena.replace(/\+/g, '');
        
        return Cadena;
    }


}