import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Headers } from '@angular/http';
import { RequestOptions } from '@angular/http';

import 'rxjs/add/operator/map';

@Injectable()
export class ChequeoFormularioService {
    constructor(private _http: Http) { }


    buscarChequeosTipo(UrlApi, IdTiposProcesos) {
        var params = '';
        return this._http.get(UrlApi + 'odata/ChequeoTipoes?$select=*,Chequeos/Nombre&$expand=Chequeos&$filter=IdTiposProcesos eq ' + IdTiposProcesos).map(res => res.json());
    }

}