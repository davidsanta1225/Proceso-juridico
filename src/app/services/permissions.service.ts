import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Headers } from '@angular/http';
import { RequestOptions } from '@angular/http';

import 'rxjs/add/operator/map';                            

@Injectable()
export class PermissionsService {
    constructor(private _http: Http) { }


    /*

        SavePermissions(UrlServicio, PermissionsObject){
        
        var Datos = new FormData;
        Datos.append('Name', PermissionsObject.Name);
        Datos.append('MyActivities', PermissionsObject.MyActivities);
        Datos.append('MyPrices', PermissionsObject.MyPrices);
        Datos.append('Indeterminant', PermissionsObject.Indeterminant);
        Datos.append('Users', PermissionsObject.Users);
        Datos.append('Permissions', PermissionsObject.Permissions);
        Datos.append('Roles', PermissionsObject.Roles);
        Datos.append('Customers', PermissionsObject.Customers);
        
        
        
        var headers = new Headers();
            
        var options = new RequestOptions({ headers: headers });

        return this._http.post(UrlServicio + 'PermissionsObject', Datos, options).map(res => res.json());
    }

    */



    //Metodo que elimanara caracteres especiales 

    ValidarCadena(Cadena){

        console.log(Cadena);

        Cadena = Cadena.replace(/'/g, '');
        Cadena = Cadena.replace(/"/g, '');
        Cadena = Cadena.replace(/%/g, '');
        Cadena = Cadena.replace(/&/g, '');
        Cadena = Cadena.replace(/\$/g, '');
        Cadena = Cadena.replace(/!/g, '');
        Cadena = Cadena.replace(/¿/g, '');
        Cadena = Cadena.replace(/\*/g, '');
        Cadena = Cadena.replace(/\?/g, '');
        Cadena = Cadena.replace(/º/g, '');
        Cadena = Cadena.replace(/\//g, '');
        Cadena = Cadena.replace(/\+/g, '');
        
        return Cadena;
    }


}