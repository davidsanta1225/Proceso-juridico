import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Headers } from '@angular/http';
import { RequestOptions } from '@angular/http';

import 'rxjs/add/operator/map';

@Injectable()
export class ListachequeoService {
    constructor(private _http: Http) { }




    SaveListaChequeo(UrlServicio, ListaChequeoObject) {


        var Datos = { Nombre: ListaChequeoObject.Nombre };

        var headers = new Headers();

        return this._http.post(UrlServicio + 'odata/Chequeos', Datos).map(res => res.json());
    }


    BorrarChequeosTipo(UrlServicio, IdChequeoTipo) {

        return this._http.delete(UrlServicio + 'odata/ChequeoTipoes(' + IdChequeoTipo + ')').map(res => res.json());

    }



    UpadateListaChequeo(UrlServicio, ListaChequeoObject) {


        var Datos = { IdChequeo: ListaChequeoObject.IdChequeo, Nombre: ListaChequeoObject.Nombre };

        var headers = new Headers();

        return this._http.put(UrlServicio + 'odata/Chequeos(' + ListaChequeoObject.IdChequeo + ')', Datos).map(res => res.json());
    }




    SearchChequeo(UrlApi) {
        var params = '';
        return this._http.get(UrlApi + 'odata/Chequeos' + params).map(res => res.json());
    }




    // guardar tipo x chequeo


    ChequeoTiposProcesos(UrlServicio, IdTiposProcesos, IdChequeo) {


        var Datos = { IdTiposProcesos: IdTiposProcesos, IdChequeo: IdChequeo };

        var headers = new Headers();

        return this._http.post(UrlServicio + 'odata/ChequeoTipoes', Datos).map(res => res.json());
    }



    buscarChequeoTipos(UrlApi, IdTiposProcesos) {
        var params = '';
        return this._http.get(UrlApi + 'odata/ChequeoTipoes?$filter=IdTiposProcesos eq ' + IdTiposProcesos).map(res => res.json());
    }
}
