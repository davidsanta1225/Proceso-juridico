import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Headers } from '@angular/http';
import { RequestOptions } from '@angular/http';


import 'rxjs/add/operator/map';

@Injectable()
export class HistorialService {
    constructor(private _http: Http) { }


    GuardarHistorial(UrlServicio, HistorialObject) {


        var Datos = { IdUsuario: HistorialObject.IdUsuario, IdEstadoFormulario: HistorialObject.IdEstadoFormulario, IdFormulario: HistorialObject.IdFormulario, Observacion: HistorialObject.Observacion, FechaCreacion: HistorialObject.FechaCreacion };

        var headers = new Headers();

        return this._http.post(UrlServicio + 'odata/Historias', Datos).map(res => res.json());
    }

    BuscarHistorial(UrlApi) {
        return this._http.get(UrlApi + 'odata/Historias').map(res => res.json());
    }

    BuscarUsuarioXHistorial(UrlApi, IdUsuario) {

        return this._http.get(UrlApi + 'odata/Usuarios?$filter=IdUsuario eq ' + IdUsuario).map(res => res.json());
    }

    BuscarEstadoXHistorial(UrlApi, IdEstadoFormulario) {

        return this._http.get(UrlApi + 'odata/EstadosFormularios?$filter=IdEstadoFormulario eq ' + IdEstadoFormulario).map(res => res.json());
    }

    BuscarFormularioxHistorial(UrlApi, IdFormulario) {

        return this._http.get(UrlApi + 'odata/Formularios?$filter=IdFormulario eq ' + IdFormulario).map(res => res.json());
    }


}