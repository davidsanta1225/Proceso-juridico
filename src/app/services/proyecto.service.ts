import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Headers } from '@angular/http';
import { RequestOptions } from '@angular/http';


import 'rxjs/add/operator/map';

@Injectable()
export class ProyectoService {
    constructor(private _http: Http) { }



    GuardarProyecto(UrlServicio, ProyectoObject) {


        var Datos = { IdContratos: ProyectoObject.IdContratos, NombreProyecto: ProyectoObject.NombreProyecto, Interventor: ProyectoObject.Interventor, Representador: ProyectoObject.Representador };

        var headers = new Headers();

        return this._http.post(UrlServicio + 'odata/Proyectos', Datos).map(res => res.json());
    }


    BuscarContrato(UrlApi) {

        return this._http.get(UrlApi + 'odata/Contratos').map(res => res.json());
    }


    ActualizarProyecto(UrlServicio, ProyectoObject) {


        var Datos = { IdContratos: ProyectoObject.IdContratos, NombreProyecto: ProyectoObject.NombreProyecto, Interventor: ProyectoObject.Interventor, Representador: ProyectoObject.Representador, IdProyectos: ProyectoObject.IdProyectos };

        var headers = new Headers();

        return this._http.put(UrlServicio + 'odata/Proyectos(' + ProyectoObject.IdProyectos + ')', Datos).map(res => res.json());

    }

    BuscarProyecto(UrlApi) {

        return this._http.get(UrlApi + 'odata/Proyectos').map(res => res.json());
    }

    BuscarNombreContrato(UrlApi, IdContrato) {

        return this._http.get(UrlApi + 'odata/Contratos?$filter=IdContrato eq ' + IdContrato).map(res => res.json());
    }


}