import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Headers } from '@angular/http';
import { RequestOptions } from '@angular/http';

import 'rxjs/add/operator/map';                            

@Injectable()
export class EtapasProcesalesService {
    constructor(private _http: Http) { }


// Guardar 

    SaveEtapasProcesales(UrlServicio, EtapasProcesalesObject){

        
        //var Datos = {Nombre: EtapasProcesalesObject.Nombre, Terminos: EtapasProcesalesObject.Terminos};
        var Datos = { Nombre: EtapasProcesalesObject.Nombre, Terminos: EtapasProcesalesObject.Terminos == '' ? 0 : EtapasProcesalesObject.Terminos};
            
   
        
        var headers = new Headers();

        return this._http.post(UrlServicio + 'odata/EstadosProcesales', Datos).map(res => res.json());
    }


    // editar

        UpadateEtapasProcesales(UrlServicio, EtapasProcesalesObject){


        var Datos = {IdEstados: EtapasProcesalesObject.IdEstados, Nombre: EtapasProcesalesObject.Nombre, Terminos: EtapasProcesalesObject.Terminos == '' ? 0 : EtapasProcesalesObject.Terminos };
        
        var headers = new Headers();
            
        return this._http.put(UrlServicio + 'odata/EstadosProcesales('+EtapasProcesalesObject.IdEstados+')', Datos).map(res => res.json());
    }




    // buscar tipo de proceso

        SearchEtapasProceso(UrlApi){
        var params='';
        return this._http.get(UrlApi + 'odata/EstadosProcesales' + params).map(res => res.json());
    }
}