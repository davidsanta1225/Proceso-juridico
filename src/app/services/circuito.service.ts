import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Headers } from '@angular/http';
import { RequestOptions } from '@angular/http';


import 'rxjs/add/operator/map';

@Injectable()
export class CircuitoService {
    constructor(private _http: Http) { }



    BuscarCircuito(UrlApi) {
        var params = '';

        return this._http.get(UrlApi + 'odata/Circuitoes' + params).map(res => res.json());
    }

    BuscarCircuitoExistente(UrlApi, Nombre) {
        var params = '';

        return this._http.get(UrlApi + "odata/Circuitoes?$filter=Nombre eq '" + Nombre + "'").map(res => res.json());
    }

    BuscarCircuitoExistenteActualizar(UrlApi, Nombre, IdCircuito) {
        var params = '';

        return this._http.get(UrlApi + "odata/Circuitoes?$filter=Nombre eq '" + Nombre + "' and IdCircuito ne "+IdCircuito+"").map(res => res.json());
    }

    


    BuscarDistritoCircuito(UrlApi) {
        var params = '';

        return this._http.get(UrlApi + 'odata/Distritoes' + params).map(res => res.json());
    }


    GuardarCircuito(UrlServicio, CircuitoObject) {


        var Datos = { Nombre: CircuitoObject.Nombre, IdDistrito: CircuitoObject.IdDistrito };

        var headers = new Headers();

        return this._http.post(UrlServicio + 'odata/Circuitoes', Datos).map(res => res.json());
    }



    ActualizarCircuito(UrlServicio, CircuitoObject) {


        var Datos = { Nombre: CircuitoObject.Nombre, IdDistrito: CircuitoObject.IdDistrito, IdCircuito: CircuitoObject.IdCircuito };

        var headers = new Headers();

        return this._http.put(UrlServicio + 'odata/Circuitoes(' + CircuitoObject.IdCircuito + ')', Datos).map(res => res.json());

    }



    BuscarCircuitoXDistrito(UrlApi, IdDistrito) {
        var params = '';

        return this._http.get(UrlApi + 'odata/Circuitoes?$filter=IdDistrito eq ' + IdDistrito).map(res => res.json());
    }




    BuscarDistritoXCircuito(UrlApi, IdCircuito) {
        var params = '';

        return this._http.get(UrlApi + 'odata/Circuitoes?$filter=IdCircuito eq ' + IdCircuito).map(res => res.json());
    }


}