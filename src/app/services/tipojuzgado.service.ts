import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Headers } from '@angular/http';
import { RequestOptions } from '@angular/http';


import 'rxjs/add/operator/map';

@Injectable()
export class TipoJuzgadoService {

    constructor(private _http: Http) { }

        BuscarTipoJuzgado(UrlApi) {
        var params = '';

        return this._http.get(UrlApi + 'odata/TipoJuzgadoes' + params).map(res => res.json());
    }


    buscarJuzgadoTipoNaturaleza(UrlApi, IdTipoJuzgado) {
        var params = '';
        return this._http.get(UrlApi + 'odata/TipoJuzgadoes?$filter=IdTipoJuzgado eq ' + IdTipoJuzgado).map(res => res.json());
    }
}