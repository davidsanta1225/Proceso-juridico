import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Headers } from '@angular/http';
import { RequestOptions } from '@angular/http';


import 'rxjs/add/operator/map';

@Injectable()
export class ContratoService {
    constructor(private _http: Http) { }



    BuscarPersona(UrlApi) {
        var params = '';

        return this._http.get(UrlApi + 'odata/Personas' + params).map(res => res.json());
    }


    GuardarContrato(UrlServicio, ContratoObject) {


        var Datos = { IdPersona: ContratoObject.IdPersona, Contrato: ContratoObject.Contrato, Descripcion: ContratoObject.Descripcion };

        var headers = new Headers();

        return this._http.post(UrlServicio + 'odata/Contratos', Datos).map(res => res.json());
    }


    BuscarContrato(UrlApi) {

        return this._http.get(UrlApi + 'odata/Contratos').map(res => res.json());
    }


    BuscarNumeroDePersona(UrlApi, IdPersona) {

        return this._http.get(UrlApi + 'odata/Personas?$filter=IdPersona eq ' + IdPersona).map(res => res.json());
    }


    ActualizarContrato(UrlServicio, ContratoObject) {


        var Datos = { Contrato: ContratoObject.Contrato, Descripcion: ContratoObject.Descripcion, IdPersona: ContratoObject.IdPersona, IdContrato: ContratoObject.IdContrato };

        var headers = new Headers();

        return this._http.put(UrlServicio + 'odata/Contratos(' + ContratoObject.IdContrato + ')', Datos).map(res => res.json());

    }



}