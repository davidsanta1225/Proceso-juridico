import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Headers } from '@angular/http';
import { RequestOptions } from '@angular/http';


import 'rxjs/add/operator/map';

@Injectable()
export class SalarioService {


    constructor(private _http: Http) { }


    BuscarSalario(UrlApi) {
        var params = '';

        return this._http.get(UrlApi + 'odata/smlvs' + params).map(res => res.json());
    }


    GuardarSalario(UrlServicio, SalarioObject) {


        var Datos = { Ano: SalarioObject.Ano, Valor: SalarioObject.Valor };

        var headers = new Headers();

        return this._http.post(UrlServicio + 'odata/smlvs', Datos).map(res => res.json());
    }


    ActualizarSalario(UrlServicio, SalarioObject) {


        var Datos = { Idsmlv: SalarioObject.Idsmlv, Ano: SalarioObject.Ano, Valor: SalarioObject.Valor };

        var headers = new Headers();

        return this._http.put(UrlServicio + 'odata/smlvs(' + SalarioObject.Idsmlv + ')', Datos).map(res => res.json());

    }

    AlgoritoCuantia(UrlServicio, valor) {

        var fecha = new Date();

        var anio = fecha.getFullYear();

        var Datos = { valor: parseInt(valor), fechaAno: anio };
        return this._http.post(UrlServicio + 'Algoritmos/Cuantia', Datos).map(res => res.json());

    }
/*
    ConsultarCuantia(UrlApi) {
        var params = '';

        return this._http.post(UrlApi + 'Algoritmos/Cuantia' + params).map(res => res.json());
    }*/


}