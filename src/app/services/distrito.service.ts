import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Headers } from '@angular/http';
import { RequestOptions } from '@angular/http';


import 'rxjs/add/operator/map';

@Injectable()
export class DistritoService {
    constructor(private _http: Http) { }



    BuscarDistrito(UrlApi) {
        var params = '';

        return this._http.get(UrlApi + 'odata/Distritoes').map(res => res.json());
    }

    BuscarDistritoExistente(UrlApi, Nombre) {
        var params = '';

        return this._http.get(UrlApi + "odata/Distritoes?$filter=Nombre eq '"+Nombre+"'").map(res => res.json());
    }


    BuscarDistritoExistenteActualizar(UrlApi, Nombre, IdDistrito) {
        var params = '';

        return this._http.get(UrlApi + "odata/Distritoes?$filter=Nombre eq '" + Nombre + "' and IdDistrito ne " + IdDistrito + " ").map(res => res.json());
    }

    GuardarDistrito(UrlServicio, DistritoObject) {


        var Datos = { Nombre: DistritoObject.Nombre };

        var headers = new Headers();

        return this._http.post(UrlServicio + 'odata/Distritoes', Datos).map(res => res.json());
    }


    ActualizarDistrito(UrlServicio, DistritoObject) {


        var Datos = { IdDistrito: DistritoObject.IdDistrito, Nombre: DistritoObject.Nombre };

        var headers = new Headers();

        return this._http.put(UrlServicio + 'odata/Distritoes(' + DistritoObject.IdDistrito + ')', Datos).map(res => res.json());

    }


}