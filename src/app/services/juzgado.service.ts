import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Headers } from '@angular/http';
import { RequestOptions } from '@angular/http';

import 'rxjs/add/operator/map';

@Injectable()
export class JuzgadoService {
    constructor(private _http: Http) { }



    BuscarJuzgadoxNaturaleza(UrlApi, IdNaturaleza) {
        var params = '';
        return this._http.get(UrlApi + 'odata/Juzgadoes?$filter=IdNaturaleza eq ' + IdNaturaleza).map(res => res.json());
    }


    GuardarJuzgados(UrlServicio, JuzgadosObject) {


        var Datos = {  IdMunicipio: JuzgadosObject.IdMunicipio, Circuito: JuzgadosObject.Circuito, Juzgado1: JuzgadosObject.Descripcion, IdNaturaleza:JuzgadosObject.IdNaturaleza, Juez: JuzgadosObject.Juez };

        var headers = new Headers();

        return this._http.post(UrlServicio + 'odata/Juzgadoes', Datos).map(res => res.json());
    }


    ActualizarJuzgados(UrlServicio, JuzgadosObject) {


        var Datos = {IdMunicipio: JuzgadosObject.IdMunicipio, Circuito: JuzgadosObject.Circuito, Juzgado1: JuzgadosObject.Descripcion, Juez: JuzgadosObject.Juez, IdNaturaleza:JuzgadosObject.IdNaturaleza, IdJuzgado: JuzgadosObject.IdJuzgado };

        var headers = new Headers();

        return this._http.put(UrlServicio + 'odata/Juzgadoes(' + JuzgadosObject.IdJuzgado + ')', Datos).map(res => res.json());

    }

    //     buscarJuzgadoCompetente(UrlApi) {
    //     var params = '';
    //     return this._http.get(UrlApi + 'odata/Juzgadoes').map(res => res.json());

    //     buscarJuzgadoCompetente(UrlApi) {
    //     var params = '';
    //     return this._http.get(UrlApi + 'odata/Juzgadoes').map(res => res.json());
    // }


        BuscarNaturaleza(UrlApi) {
        var params = '';
        return this._http.get(UrlApi + 'odata/Naturalezas').map(res => res.json());
    }



    
    AlgoritmoJuzgadoCompetente(UrlServicio, ProcessObject) {

        var fecha = new Date();

        var anio = fecha.getFullYear();

        var Datos = {Cuantia:ProcessObject.Cuantia,idMunicipio: ProcessObject.Municipio,  };
        return this._http.post(UrlServicio + 'Algoritmos/Juzgados', Datos).map(res => res.json());

    }



}