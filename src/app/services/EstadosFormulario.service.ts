import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Headers } from '@angular/http';
import { RequestOptions } from '@angular/http';

import 'rxjs/add/operator/map';

@Injectable()
export class PdteServices {
    constructor(private _http: Http) { }


    PendientesFormulario(UrlApi, IdEstadoFormulario) {
        var params = '';
        return this._http.get(UrlApi + 'odata/PdtesActividades?$filter=IdEstadoFormulario eq ' + IdEstadoFormulario).map(res => res.json());
    }


    GuardarPendientes(UrlServicio, PendientesObject, IdEstadoFormulario, IdFormulario){

        var fecha = new Date();

        var Datos = {
                        IdUsuario: 1,
                        Actividad: PendientesObject.Actividad, 
                        IdEstadoFormulario:IdEstadoFormulario, 
                        IdFormulario: IdFormulario,
                        FechaCreacion: fecha, 
                        FechaRecordatorio: new Date(PendientesObject.FechaRecordatorio),
                        Observacion: PendientesObject.Observacion
                    };

        console.log(Datos);
        
        var headers = new Headers();
            
        return this._http.post(UrlServicio + 'odata/PdtesActividades', Datos).map(res => res.json());
    }



}

