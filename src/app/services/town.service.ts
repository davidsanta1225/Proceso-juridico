import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Headers } from '@angular/http';
import { RequestOptions } from '@angular/http';


import 'rxjs/add/operator/map';

@Injectable()
export class TownService {
    constructor(private _http: Http) { }



    BuscarMunicipio(UrlApi) {
        var params = '';

        return this._http.get(UrlApi + 'odata/Municipios' + params).map(res => res.json());
    }

    BuscarMunicipioExistente(UrlApi, Nombre) {
        var params = '';

        return this._http.get(UrlApi + "odata/Municipios?$filter=Nombre eq '" + Nombre + "'").map(res => res.json());
    }

    BuscarMunicipioExistenteActualizar(UrlApi, Nombre, IdMunicipio) {
        var params = '';

        return this._http.get(UrlApi + "odata/Municipios?$filter=Nombre eq '" + Nombre + "' and IdMunicipio ne " + IdMunicipio + " ").map(res => res.json());
    }


    GuardarMunicipio(UrlServicio, MunicipioObject) {


        var Datos = { Nombre: MunicipioObject.Municipio, IdCircuito: MunicipioObject.IdCircuito };

        var headers = new Headers();

        return this._http.post(UrlServicio + 'odata/Municipios', Datos).map(res => res.json());
    }

    BuscarCircuitoMunicipio(UrlApi) {
        var params = '';

        return this._http.get(UrlApi + 'odata/Circuitoes' + params).map(res => res.json());
    }



    ActualizarMunicipio(UrlServicio, MunicipioObject) {


        var Datos = { IdMunicipio: MunicipioObject.IdMunicipio, Nombre: MunicipioObject.Municipio, IdCircuito: MunicipioObject.IdCircuito };

        var headers = new Headers();

        return this._http.put(UrlServicio + 'odata/Municipios(' + MunicipioObject.IdMunicipio + ')', Datos).map(res => res.json());

    }


    BuscarMunicipioXCircuito(UrlApi, IdCircuito) {
        var params = '';

        return this._http.get(UrlApi + 'odata/Municipios?$filter=IdCircuito eq ' + IdCircuito).map(res => res.json());
    }


    BuscarCircuitoXMunicipio(UrlApi, IdMunicipio) {

        var params = '';

        return this._http.get(UrlApi + 'odata/Municipios?$filter=IdMunicipio eq ' + IdMunicipio).map(res => res.json());
    }






}