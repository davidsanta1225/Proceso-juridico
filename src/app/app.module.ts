import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { HttpModule} from '@angular/http';

import { AppComponent }  from './controllers/app.component';

import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';

import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';

import {DataTableModule,SharedModule} from 'primeng/primeng';






import { HomeComponent } from './controllers/home.component';
import { RegisterComponent } from './controllers/register.component';
import { CustomerComponent } from './controllers/customer.component';
import { UsersComponent } from './controllers/users.component';
import { PermissionsComponent } from './controllers/permissions.component';
import { StepsComponent } from './controllers/steps.component';
import { StepsPComponent } from './controllers/stepsp.component';
import { RresponsableComponent } from './controllers/r-responsable.component';
import { LogComponent } from './controllers/log.component';
import { CourtComponent } from './controllers/court.component';
import { MyactivitiesComponent } from './controllers/myactivities.component';
import { ActivitiestateComponent } from './controllers/activitiestate.component';
import { ReportregisterComponent } from './controllers/reportregister.component';
import { TownComponent } from './controllers/town.component';
import { PresentacionComponent } from './controllers/presentacionDemanda.component';
import { DocumentComponent } from './controllers/document.component';
import { TypeProcessComponent } from './controllers/typeProcess.component';
import { AuditoriaComponent } from './controllers/auditoria.component';











import { CarouselModule } from 'ngx-bootstrap/carousel';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';


import swal from 'sweetalert2';



@NgModule({
  imports:      [ 
                  BrowserModule,
                  AppRoutingModule,
                  HttpModule,
                  FormsModule,
                  CarouselModule.forRoot(),
                  CollapseModule.forRoot(),
                  ModalModule.forRoot(),
                  TabsModule.forRoot(),
                  MultiselectDropdownModule,
                  DataTableModule,
                  SharedModule,
                ],
  declarations: [ 
                  AppComponent,
                  HomeComponent,
                  RegisterComponent,
                  CustomerComponent,
                  UsersComponent,
                  PermissionsComponent,
                  StepsComponent,
                  StepsPComponent,
                  RresponsableComponent,
                  LogComponent,
                  CourtComponent,
                  MyactivitiesComponent,
                  ActivitiestateComponent,
                  ReportregisterComponent,
                  TownComponent,
                  PresentacionComponent,
                  DocumentComponent,
                  TypeProcessComponent,
                  AuditoriaComponent,
                  
                  
                  


                ],
  providers: [],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }