export class Users {

constructor(
    public Nombre:string,
    public Apellidos:string,
    public Documentos:string,
    public Cargo:string,
    public CorreoElectronico:string,
    public Password:string,
    public IdRol:string,
    public ConfirmPassword:string,
    public IdUsuario?:number,

){}

}