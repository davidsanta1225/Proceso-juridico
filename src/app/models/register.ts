export class Register {

    constructor(
        public NumeroProceso:string,
        public Proyecto:string,
        public IdPersona:string,
        public Demandante:string,
        public Demandado:string,
        public Radicado:string,        
        public Valor:string,        
        public Cuantia:string,        
        public Municipio:string, 
        public JuzgadoConocimiento: string,
        public Juez:string,                       
        public Estimativo:string, 
        public IdTiposProcesos:string, 
        public Responsable:string,
        public ResponsableSucesor:string,  
        public IdFormulario?:string, 
        

    ){}

}