export class Juzgados{

    constructor(
        public IdMunicipio:string,
        public Circuito:boolean,
        public Descripcion:string,
        public Juez:string,
        public IdNaturaleza:string,
        public IdJuzgado?: string,
    ){}
}