export class TipoJuzgado{

    constructor(
        public Nombre:string,
        public Naturaleza:string,
        public IdTipoJuzgado?:number
    ){}
}