export class Pendientes{
constructor(
    public IdUsuario:string,
    public IdEstadoFormulario:string,
    public IdFormulario:string,
    public Observacion:string,
    public Actividad:string,
    public FechaRecordatorio:string,
    public IdPdte?:string,
){}

}