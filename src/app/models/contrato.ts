export class Contrato{

    constructor(
        public IdPersona:string,
        public Contrato:string,
        public Descripcion:string,
        public IdContrato?:string,
    ){}
}