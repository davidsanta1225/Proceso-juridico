export class Proyecto {

    constructor(

        public IdContratos:string,
        public NombreProyecto:string,
        public Interventor:string,
        public Representador:string,
        public IdProyectos?:string,
    ){}
}