export class Customer {

    constructor(
        public Nombre:string,
        public NumeroIdentidad :string,
        public Correo :string,
        public Direccion:string,
        public IdPersona?: number
    ){}
}