export class Permissions {

    constructor(

        public Name: string,
        public MyActivities: string,
        public MyPrices: string,
        public Indeterminant: string,
        public Users: string,        
        public Roles: string,
        public Permissions: string,
        public Customers: string,
        
        
    ){}
}