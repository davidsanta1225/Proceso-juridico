export class Historial{

    constructor(

        public IdUsuario:string,
        public IdEstadosFormulario:string,
        public IdFormulario:string,
        public FechaCreacion:string,
        public Observacion:string

    ){}
}